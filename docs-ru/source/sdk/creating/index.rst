=================
Создание контента
=================

Varwin SDK позволяет создавать объекты и шаблоны сцен для платформы Varwin.

Данный раздел рассказывает о создании контента в редакторе Unity, об особенностях программирования логики объектов для платформы Varwin читайте в разделе `Scripting. <https://varwin.readthedocs.io/ru/0.7.0/sdk/scripting/index.html>`__

.. toctree::
    :titlesonly:
    :hidden:

    object
    complex-object
    scene-template