==========================
Атрибуты методов и событий
==========================

`Подробнее:` `Общие свойства атрибутов <https://varwin.readthedocs.io/ru/0.7.0/sdk/scripting/blockly/general-properties.html>`__

Checker
=======

Используются для методов, возвращающих логический тип.

Пример кода
-----------

.. code:: c#

    public class Cube : VarwinObject
    {
        [Checker(English: "check if zero")]
        public bool ZeroChecker(int i)
        {
            return i == 0;
        }
    }

Пример блока
------------

.. image:: blockly-images/image_6.png

CheckerGroup
------------

``[CheckerGroup("group name")]``

Action
======

Используется для методов, не возвращающих значение. 

Пример кода
-----------

.. code:: c#

    public class Cube : VarwinObject
    {
        [Action(English: "debug message")]
        public void SampleAction(string message)
        {
            Debug.Log(message);
        }
    }

Пример блока
------------

.. image:: blockly-images/image_7.png

ActionGroup
-----------

``[ActionGroup("group name")]``

Function
========

Используются для методов, возвращающих значение.

Пример кода
-----------

.. code:: c#

    public class Cube : VarwinObject
    {
        [Function(English: "sum")]
        public int Add(int a, int b)
        {
            return a + b;
        }
    }

Пример блока
------------

.. image:: blockly-images/image_8.png

FunctionGroup
-------------

``[FunctionGroup("group name")]``

Event
=====

Используются для событий.

Пример кода
-----------

.. code:: c#

    public class Cube : VarwinObject
    {
        public delegate void SampleEventHandler();

        [Event(English: "sample event")]
        public event SampleEventHandler sampleEvent;

        private void Start()
        {
            sampleEvent?.Invoke();
        }
    }

Пример блока
------------

.. image:: blockly-images/image_9.png

EventGroup
----------

``[EventGroup("group name")]``

ArgsFormat
==========

Используется для форматирования вида блока.

Применим в качестве дополнительного атрибута для всех атрибутов методов и событий.

Может также сочетаться с атрибутом группы.

Например, блок, реализующий логику сложения двух чисел, без форматирования выглядит следующим образом:

.. image:: blockly-images/image_10.png

Чтобы добавить текст между аргументами необходимо использовать атрибут 

``[ArgsFormat(Locale: "locale formatting string")]``. 

Строка форматирования задается следующим образом: "{%} + {%}", где {%} - аргумент метода. Аргументы добавляются в порядке вхождения.

Пример кода 
-----------

.. code:: c#

    public class Cube : VarwinObject
    {
        [Function(English: "sum")]
        [ArgsFormat(English:"{%} + {%}")]
        public int Sum(int a, int b)
        {
            return a + b;
        }
    }

Пример блока
------------

.. image:: blockly-images/image_11.png