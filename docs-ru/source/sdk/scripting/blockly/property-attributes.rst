================
Атрибуты свойств
================

`Подробнее:` `Общие свойства атрибутов <https://varwin.readthedocs.io/ru/0.7.0/sdk/scripting/blockly/general-properties.html>`__

Variable
========

Генерирует блоки для ``public`` и ``internal`` геттера и сеттера. Для ``private`` блок сгенерирован не будет.

Пример кода
-----------

.. code:: c#

    public class Cube : VarwinObject
    {
        private string _text;

        [Variable(English: "text", Russian: "текст")]
        public string TextPanel
        {
            get => _text;
            internal set => _text = value;
        }
    }

Пример блоков
-------------

.. image:: blockly-images/image_5.png

VariableGroup
-------------

``[VariableGroup("group name")]``