========================
Общие свойства атрибутов
========================

Локализация
===========

При использовании атрибутов обязательно указываются локализованные имена блоков в следующем виде:

``[AttributeName(English: "text", Russian: "текст")]``

При этом должна присутствовать как минимум, одна локаль.

Модификаторы доступа
====================

Блоки генерируется только для сущностей с модификаторами доступа ``public`` и ``internal``.

Пример
------

Например, для атрибута свойств ``[Variable]`` код выглядит следующим образом:

.. code:: c#

    public class Cube : VarwinObject
    {
        private string _text;

        [Variable(English: "text", Russian: "текст")]
        public string TextPanel
        {
            get => _text;
            internal set => _text = value;
        }
    }

Данный код сгенерирует следующие блоки для блокли.

.. image:: blockly-images/image_0.png

где 21 - имя конкретного экземпляра объекта ``Cube``, созданного в платформе Varwin.

При переключении языка платформы Varwin вид блоков изменится в соответствии с выбранным языком.

.. image:: blockly-images/image_1.png

Group Attributes
================

Для некоторых атрибутов существуют дополнительные атрибуты, так называемые атрибуты групп, позволяющие создавать группы блоков. Блоки с одинаковыми именами группы будут группироваться в один блок с выпадающим списком.

.. image:: blockly-images/image_2.png

Такие атрибуты можно использовать только в связке с основным атрибутом.

Важно помнить, что можно группировать только блоки с одинаковыми сигнатурами (количество и типы входных параметров должны совпадать).

Например, для атрибута ``[Variable]`` атрибут групп ``[VariableGroup("group name")]``.

Пример
------

.. code:: c#

    public class Cube : VarwinObject
    {
        private string _text;
        private string _anotherText;

        [VariableGroup("panel texts")]
        [Variable(English: "text", Russian: "текст")]

        public string TextPanel
        {
            get => _text;
            set => _text = value;
        }

        [VariableGroup("panel texts")]
        [Variable(English: "another text", Russian: "другой текст")]
        internal string AnotherText
        {
            get => _anotherText;
            set => _anotherText = value;
        }
    }

Для этого кода будут сгенерированы по одному геттеры и сеттеру с возможностью выбрать интересующее свойство в выпадающем списке.

.. image:: blockly-images/image_3.png

.. image:: blockly-images/image_4.png

Список атрибутов
----------------

В Varwin определены следующие атрибуты групп:

* ActionGroup
* CheckerGroup
* EventGroup
* FunctionGroup
* VariableGroup
