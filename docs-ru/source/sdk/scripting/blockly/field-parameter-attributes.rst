=======================================
Атрибуты переменных и параметров метода
=======================================

`Подробнее:` `Общие свойства атрибутов <https://varwin.readthedocs.io/ru/0.7.0/sdk/scripting/blockly/general-properties.html>`__

Item
====

Используется для изменения отображаемого имени элементов перечисления (enum), когда enum передается в качестве параметра метода.

Пример кода

.. code:: c#

    public class Cube : VarwinObject
    {
        public enum CubeColors
        {
            [Item(English: "white")] Default,
            Red,
            Green,
            Blue
        }

        [Action(English: "Set cube color")]
        public void SetCubeColor(CubeColors color)
        {
            // set color
        }
    }

Пример блока
------------

.. image:: blockly-images/image_12.png

Связка атрибутов UseValueList - Value - ValueList
=================================================

Используется в случаях, когда в качестве параметра метода передается переменная, не являющаяся перечислением. При этом необходимо предоставить пользователю вместо поля значения выбор из списка предопределенных именованных значений.

Для пользователя выглядит точно так же как и выбор из элементов перечисления.

UseValueList
------------

Атрибут параметров метода.

При объявлении атрибута через запятую перечисляются имена списков, которые необходимо включить в выпадающий список в blockly. Списки определяются с помощью атрибутов ``[Value]`` и ``[ValueList]``.

``[UseValueList("sizes", "small sizes", "large sizes")]``

**Пример кода**

.. code:: c#

        [Action(English: "Set cube size")]
        public void SetCubeSize([**UseValueList("sizes", "small sizes","large sizes")**] Vector3 size)
        {
            // set size
        }

Value
-----

Используется для создания именованной переменной.

.. code:: c#

    [Value(English:"very small")]
    public Vector3 VerySmallSize = 0.25f * Vector3.one;

ValueList
---------

Используется для создания именованного списка значений.

Следующий код добавляет переменную "very small" к спискам “small sizes” и “sizes”.

.. code:: c#

    [ValueList("small sizes", "sizes")]
    [Value(English:"very small")]
    public Vector3 VerySmallSize = 0.25f * Vector3.one;

Пример использования связки UseValueList - Value - ValueList
============================================================

В следующем примере создается несколько именованных переменных, добавляются к различным именованным спискам и создается метод, принимающий в качестве аргумента переменную.

С помощью атрибута ``[UseValueList]`` в блокли появляется возможность выбрать переменную из списка.

Пример кода
-----------

.. code:: c#

    public class Cube : VarwinObject
    {
        [ValueList("small sizes", "sizes")]
        [Value(English:"very small")]
        public Vector3 VerySmallSize = 0.25f * Vector3.one;

        [ValueList("small sizes", "sizes")]
        [Value(English:"small")]
        public Vector3 SmallSize = 0.5f * Vector3.one;

        [ValueList("sizes")]
        [Value(English:"medium")]
        public Vector3 MediumSize = 1.0f * Vector3.one;

        [ValueList("large sizes", "sizes")]
        [Value(English:"large")]
        public Vector3 LargeSize = 1.5f * Vector3.one;

        [ValueList("large sizes", "sizes")]
        [Value(English:"very large")]
        public Vector3 VeryLargeSize = 2.0f * Vector3.one;

        [Action(English: "Set cube size")]
        public void SetCubeSize([UseValueList("sizes", "small sizes", "large sizes")] Vector3 size)
        {
            // set size
        }
    }

Пример блока
------------

.. image:: blockly-images/image_13.png
