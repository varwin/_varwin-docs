==================
Обработка коллизий
==================

Varwin SDK предоставляет интерфейс для обработки коллизий с объектами.

IColliderAware
--------------

``IColliderAware`` реализуется для так называемых зон, которые можно использовать для того, чтобы определять нахождение игрока или предметов в какой-либо области. Следовательно, при использовании интерфейса необходимо, чтобы коллайдер объекта был триггером.

.. code:: c#
 
    public interface IColliderAware
    {
        void OnObjectEnter(Wrapper[] wrappers);
        void OnObjectExit(Wrapper[] wrappers);
    } 


``OnObjectEnter`` вызывается, когда Varwin объект сталкивается с данным объектом. Аналог Unity метода ``OnTriggerEnter`` для Varwin объектов. 

``OnObjectExit`` вызывается, когда Varwin объекты перестают взаимодействовать. Аналог Unity метода ``OnTriggerExit`` для Varwin объектов. 

Как создать зону
----------------

В библиотеке объектов Varwin платформы существует объект Custom zone, который виден в режиме редактирования и невидим в режиме просмотра. Custom zone предоставляет события на вход объектов в зону и выход объектов из зоны.

Данный урок пошагово рассказывает о процессе создания объекта зоны, реагирующей на появление объектов. Для изменения видимости зоны в зависимости от режима работы ``GameMode`` достаточно реализовать интерфейс `IGameModeSubscriber <https://varwin.readthedocs.io/ru/0.7.0/sdk/scripting/mode-switching.html>`__.

.. image:: images-handling-collisions/image_0.png


**Шаг 1.**  Создать куб, добавить его на слой Zones. 

`Подробнее:` `Varwin слои. <https://varwin.readthedocs.io/ru/0.7.0/sdk/creating/object.html>`__

.. image:: images-handling-collisions/image_1.png

Добавить Kinematic Rigidbody

.. image:: images-handling-collisions/image_2.png

Установить на коллайдере Is Trigger

.. image:: images-handling-collisions/image_3.png

Создать прозрачный материал и назначить на куб

.. image:: images-handling-collisions/image_4.png


**Шаг 2.** Создать Varwin объект. 

`Подробнее :` `Создание объектов. <https://varwin.readthedocs.io/ru/0.7.0/sdk/scripting/unity-settings.html>`__

Теперь объект в сцене стал префабом, и на нем появились скрипты ``VarwinObjectDescriptor`` и ``TutorialZone``.

``TutorialZone`` - основной скрипт объекта, наследник ``VarwinObject``.

**Шаг 3.** Реализация логики.

Открыть класс ``TutorialZone``, реализовать интерфейс ``IColliderAware``. 

Создать делегат  ``ZoneObjectHandler`` - обработчик событий.

Создать события ``ObjectEnterEvent`` и ``ObjectExitEvent``.

Пометить события атрибутами, позволяющими использовать их в blockly. Ссылка: подробнее про атрибуты.

Создать массив врапперов объектов, содержащихся в зоне в данный момент containedObjects.

Реализовать методы ``OnObjectEnter`` и ``OnObjectExit`` (см. код). Данные методы получают массив врапперов объектов, которые находятся в зоне в данный момент. Перед тем, как обновлять локальный массив врапперов необходимо сравнить локальный и обновленный массивы и вызвать соответствующее событие в случае изменения.

.. code:: c#
 
    public class TutorialZone : VarwinObject, IColliderAware
    {
        public delegate void ZoneObjectHandler(Wrapper wrapper);

        [EventGroup("collision events")]
        [Event(English: "object entered zone")]
        public event ZoneObjectHandler ObjectEnterEvent;

        [EventGroup("collision events")]
        [Event(English: "object exited zone")]
        public event ZoneObjectHandler ObjectExitEvent;

        private Wrapper[] containedObjects;

        public void OnObjectEnter(Wrapper[] wrappers)
        {
            foreach (Wrapper obj in wrappers)
            {
                if(containedObjects == null || !containedObjects.ToList().Contains(obj))
                {
                    ObjectEnterEvent?.Invoke(obj);
                }
            }
            containedObjects = wrappers;
        }

        public void OnObjectExit(Wrapper[] wrappers)
        {
            foreach (Wrapper obj in containedObjects)
            {
                if (wrappers == null || !wrappers.ToList().Contains(obj))
                {
                    ObjectExitEvent?.Invoke(obj);
                }
            }
            containedObjects = wrappers;
        }
    }


**Шаг 4.** Сбилдить объект. 

`Подробнее :` `Билд объектов. <https://varwin.readthedocs.io/ru/0.7.0/sdk/building/building.html>`__

**Шаг 5.** Загрузить объект в Varwin RMS. Ссылка: подробнее про загрузку контента в RMS.

`Подробнее :` `Импорт контента в RMS. <https://varwin.readthedocs.io/ru/0.7.0/platform/working-with-objects/importing-exporting-content/importing.html>`__

Добавить на сцену созданную зону, лампочку и любой объект, который можно переносить. Ссылка: Подробнее про редактирование сцены.

Подписаться в блокли на созданные события - при попадании в зону объекта лампочка включается, при выходе - выключается.

.. image:: images-handling-collisions/image_5.png


**Шаг 6.** Тестирование.

Запустить режим предпросмотра или просмотра.

Взять добавленный объект, войти с ним в зону - лампочка включилась, выйти из зона - лампочка выключилась. Все работает!
