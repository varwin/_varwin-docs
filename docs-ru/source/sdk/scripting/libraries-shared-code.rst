=============================
Переиспользование компонентов
=============================

Varwin SDK использует `.admdef-файлы <https://docs.unity3d.com/2018.4/Documentation/Manual/ScriptCompilationAssemblyDefinitionFiles.html>`__ при создании и сборки объектов. Поэтому, любые дополнительные скрипты на объекте (помимо добавленных автоматически) при билде объекта будут проигнорированы.

Данная особенность инструмента вызывает известные проблемы, связанные с переиспользованием кода.
Для того, чтобы использовать одни и те же классы в разных объектах, необходимо создать библиотеку, содержащую эти классы.

.. warning:: После изменения библиотеки необходимо перебилдить все объекты, использующие данную библиотеку.

Данный урок пошагово рассказывает о процессе создания переиспользуемых компонентов.

**Шаг 1.** Создать директорию, в которой будут находиться классы библиотеки.

.. image:: libraries-images/image_0.png

**Шаг 2.** Внутри директории создать .asmdef-файл. Его имя должно совпадать с именем namespace, в котором находятся необходимые классы.

.. image:: libraries-images/image_1.png
    :scale: 75%

В разделе Assembly Definition References добавить ссылку на библиотеку ядра - VarwinCore.

.. image:: libraries-images/image_2.png
    :scale: 75%

**Шаг 3.** Добавить в директорию необходимые классы. Они все должны находиться в одном namespace с уникальным именем, обозначающим библиотеку.

.. image:: libraries-images/image_3.png

.. code:: c#

     using Varwin;
     using UnityEngine;
     using Varwin.Public;
     
     namespace Switch.v1;
     {
        [RequireComponent(typeof(Rigidbody))]
        [RequireComponent(typeof(BoxCollider))]
        [RequireComponent(typeof(Animator))]
        public class SwitchBehaviour : MonoBehaviour, IUseStartAware
        {
            public bool ChangeStateOnAnimation = false;
            private Animator _animator;
            private bool _state = false;
        }
    }
		   
**Шаг 4.** В .asmdef-файле объекта, который должен ссылаться на созданную библиотеку, в разделе Assembly Definition References необходимо добавить ссылку на эту библиотеку.

.. image:: libraries-images/image_4.png
    :scale: 75%

**Шаг 5.** Теперь все классы из библиотеки можно использовать в объекте, который ссылается на нее. Таких объектов может быть сколько угодно, и во всех можно будет использовать общий код библиотеки.

.. image:: libraries-images/image_5.png
    :scale: 75%
    