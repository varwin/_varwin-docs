=============================================================
Уведомление объектов об изменении платформы или режима работы
=============================================================

Varwin SDK предоставляет интерфейсы, позволяющие реализующим их классам узнавать об изменении целевой платформы (``PlatformMode``) или режима работы (``GameMode``).

Game Mode Switching
===================

Существуют следующие режимы работы:

* Edit - режим редактирования
* View - режим просмотра
* Preview - режим предпросмотра, отличается от View тем, что можно перейти в режим редактирования.

Текущий режим работы всегда доступен при обращении к ``ProjectData.GameMode``.

ISwitchModeSubscriber
---------------------

.. code:: c#

    public interface ISwitchModeSubscriber
    {
        void OnSwitchMode(GameMode newMode, GameMode oldMode);
    } 

``OnSwitchMode`` оповещает Varwin объекты об изменении ``GameMode``.

Platform Mode Switching
=======================

Существуют следующие целевые платформы:

* VR
* Desktop

Текущая целевая платформа всегда доступна при обращении к ``ProjectData.PlatformMode``.

ISwitchPlatformModeSubscriber
-----------------------------

.. code:: c#

    public interface ISwitchPlatformModeSubscriber
    {
        void OnSwitchPlatformMode(PlatformMode newPlatformMode, PlatformMode oldPlatformMode);
    } 

``OnSwitchPlatformMode`` оповещает Varwin объекты об изменении ``PlatformMode``.