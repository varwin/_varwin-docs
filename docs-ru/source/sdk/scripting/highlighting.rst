==================
Подсветка объектов
==================

Подсветка объектов включается при наведении контроллера на объект (действие touch).

.. image:: highlighting-images/image_0.gif


Чтобы на объекте работала подсветка, один из его компонентов должен реализовывать интерфейс ``IHighlightAware``. 

По умолчанию - при отсутствии эффекта подсветки на Varwin объекте, Varwin платформа добавляет компоненты, реализующие подсветку объекта с дефолтными настройками, при этом подсвечиваться будет объект целиком.

.. image:: highlighting-images/image_1.png
   :scale: 80 %


Varwin SDK предоставляет возможность изменить эффект подсветки:

* Изменить настройки внешнего вида эффекта, такие как цвет, толщина линий и прочие;

* Выбрать, какие части объекта будут подсвечиваться.

Для работы с подсветкой Varwin SDK определяет следующие классы и интерфейс:

* ``IHighlightAware`` - интерфейс, определяющий, будет ли на объекте работать подсветка.

* ``HighlightConfig`` - класс-контейнер настроек подсветки. Реализует интерфейс ``IHighlightAware``.

* ``DefaultHighlighter`` - класс, предоставляющий дефолтные настройки подсветки;

* ``HighlightEffect`` - класс, реализующий логику подсветки.

* ``VarwinHighlightEffect`` - класс, запускающий ``HighlightEffect`` с настройками, предоставленными ``HighlightConfig``. 

* ``HighlightOverrider`` - класс, позволяющий переопределить подсвечиваемый объект.

Разработчик контента на Varwin SDK может не волноваться о классах ``HighlightEffect`` и ``VarwinHighlightEffect``.

В случае, если подсветка по умолчанию подходит к решаемой задаче, можно забыть о вышеперечисленных классах вообще.

Далее будут рассмотрены возможности изменения подсветки Varwin объектов.


Изменение настроек подсветки
============================

Интерфейс ``IHighlightAware`` определяет, будет ли на объекте работать подсветка. Предоставляет метод, возвращающий настройки эффекта подсветки.

.. code:: c#

    public interface IHighlightAware : IVarwinInputAware
    {
        HightLightConfig HightLightConfig();
    }

Класс-контейнер ``HighlightConfig`` определяет параметры эффекта подсветки.

DefaultHighlighter
------------------

Компонент, предоставляющий настройки подсветки ``HighlightConfig`` по умолчанию. Реализует ``IHighlightAware``. Добавляется к объекту автоматически.

Для изменения данных настроек необходимо написать подобный компонент и добавить его на объект. Платформа автоматически подхватит эти изменения.

.. code:: c#

    public class DefaultHightlighter : MonoBehaviour, IHighlightAware
	{
		public HightLightConfig HightLightConfig()
		{
			HightLightConfig config = new HightLightConfig(
                true, 0.3f, Color.cyan, 
				false, 0.2f, 0.1f, 0.3f, Color.cyan,
				false, 0f, 0f, Color.red,
				false, 0f, 1.0f, Color.red);

			return config;
		}
	}

Изменение подсвечиваемых частей
===============================

Varwin SDK позволяет переопределить подсветку так, чтобы подсвечивалась лишь часть объекта. Данный урок пошагово рассказывает как это сделать.

**Шаг 1.** Выделить часть сложного объекта в дочерний gameObject.

.. image:: highlighting-images/image_2.png
    :scale: 80 %

**Шаг 2.** Добавить на корневой объект Cube компонент Highlight Overrider, назначить Object To Highlight дочерний объект Sphere. 

Поскольку подсветка работает на touch, необходимо добавить Interactable Object Behaviour и отметить Is Touchable.

.. image:: highlighting-images/image_3.png
    :scale: 80 %


**Шаг 3.** Сбилдить объект. Загрузить в Varwin RMS. 

`Подробнее :` `Билд объектов. <https://varwin.readthedocs.io/ru/0.7.0/sdk/building/building.html>`__

`Подробнее :` `Импорт контента в RMS. <https://varwin.readthedocs.io/ru/0.7.0/platform/working-with-objects/importing-exporting-content/importing.html>`__


**Шаг 4.** Тестирование. При наведение на объект подсвечивается только сфера. Все работает.

.. image:: highlighting-images/image_4.gif
    :scale: 70 %