==============================
Взаимодействие с контроллерами
==============================

Varwin SDK предоставляет компонент, позволяющий легко настраивать взаимодействие контроллеров с Varwin объектом.

Для этого необходимо добавить на Varwin объект компонент Interactable Object Behaviour.

.. image:: controller-interaction-images/image_0.png


В инспекторе устанавливаются желаемые настройки взаимодействия, а также могут быть добавлены обработчики событий.

* Is Grabbable - объект можно брать в руки и перетаскивать;

* Is Usable - объект можно использовать;

* Is Touchable - контур объекта подсвечивается при нахождении контроллера в области объекта.

.. image:: controller-interaction-images/image_1.png


InteractableObjectBehaviour реализует публичные интерфейсы, необходимые для обработки событий ввода с контроллера. 

* IUseStartAware

* IUseEndAware

* IGrabStartAware

* IGrabEndAware

* ITouchStartAware

* ITouchEndAware

Эти интерфейсы могут быть реализованы разработчиком самостоятельно при написании скриптов для Varwin объектов.

IUseStartAware 
---------------

Достаточно реализовать любой из пары интерфейсов (``IUseStartAware``, ``IUseEndAware``), чтобы объект можно было использовать.

.. code:: c#

    public interface IUseStartAware : IVarwinInputAware
    {
        void OnUseStart(UsingContext context);
    }

``OnUseStart`` вызывается при нажатии триггера контроллера или левой кнопки мыши. Принимает параметр типа ``UsingContext`` - ссылка на используемый контроллер, через нее можно получить ссылку на gameObject контроллера.

IUseEndAware
------------

Достаточно реализовать любой из пары интерфейсов (``IUseStartAware``, ``IUseEndAware``), чтобы объект можно было использовать.

.. code:: c#

    public interface IUseEndAware : IVarwinInputAware
    {
        void OnUseEnd();
    }

``OnUseEnd`` вызывается при отпускании триггера контроллера или левой кнопки мыши.

IGrabStartAware 
----------------

Достаточно реализовать любой из пары интерфейсов (``IGrabStartAware``, ``IGrabEndAware``), чтобы объект можно было хватать и переносить.

Реализация данного интерфейса подразумевает, что объект можно перетаскивать.

.. code:: c#

    public interface IGrabStartAware : IVarwinInputAware
    {
        void OnGrabStart(GrabingContext context);
    }

``OnGrabStart`` вызывается при нажатии захвата (grip) контроллера или правой кнопки мыши. Принимает параметр типа ``GrabbingContext`` - ссылка на используемый контроллер, через нее можно получить ссылку на gameObject контроллера.

IGrabEndAware
-------------

Достаточно реализовать любой из пары интерфейсов (``IGrabStartAware``, ``IGrabEndAware``), чтобы объект можно было хватать и переносить.

.. code:: c#

    public interface IGrabEndAware : IVarwinInputAware
    {
        void OnGrabEnd();
    }

``OnGrabEnd`` вызывается при отпускании захвата (grip) контроллера или нажатии правой кнопки мыши (при условии, что объект уже в руке).

ITouchStartAware
----------------

Достаточно реализовать любой из пары интерфейсов (``ITouchStartAware``, ``ITouchEndAware``), чтобы объект реагировал на касания.

.. code:: c#

    public interface ITouchStartAware : IVarwinInputAware
    {
        void OnTouchStart();
    }

``OnTouchStart`` вызывается при вхождении контроллера в область объекта.

.. image:: controller-interaction-images/image_2.png
    :scale: 50 %


ITouchEndAware 
---------------

Достаточно реализовать любой из пары интерфейсов (``ITouchStartAware``, ``ITouchEndAware``), чтобы объект реагировал на касания.

.. code:: c#

    public interface ITouchEndAware : IVarwinInputAware
    {
        void OnTouchEnd();
    }

``OnTouchEnd`` вызывается при выхождении контроллера из области объекта.

Другие интерфейсы для взаимодействия с контроллерами
====================================================

IGrabPointAware
---------------

Предоставляет методы, позволяющие получить transform точки объекта, к которой присоединяется контроллер (для левого и правого контроллера соответственно). Вызывается в момент присоединения объекта к контроллеру (on grab).

.. code:: c#

    public interface IGrabPointAware
    {
        Transform GetLeftGrabPoint();
        Transform GetRightGrabPoint();
    }

.. image:: controller-interaction-images/image_3.png
    :scale: 70 %

IPointerClickAware
------------------

.. code:: c#

    public interface IPointerClickAware : IVarwinInputAware
    {
        void OnPointerClick();
    }  

``OnPointerClick`` вызывается при клике объекта лучом указки. 

.. image:: controller-interaction-images/image_4.png
    :scale: 50 %

IPointerInAware
---------------

.. code:: c#

    public interface IPointerInAware : IVarwinInputAware
    {
        void OnPointerIn();
    } 

``OnPointerIn`` вызывается при входе луча указки в область объекта.

IPointerOutAware
----------------

.. code:: c#

    public interface IPointerOutAware : IVarwinInputAware
    {
        void OnPointerOut();
    } 

``OnPointerOut`` вызывается при выходе луча указки из области объекта.

Grab Settings Component
=======================

Если необходимо, чтобы объект ложился в руку определенным образом, можно воспользоваться компонентом Grab Settings. Он позволяет задать точки к которым будет прикрепляться контроллер (для правой и левой рук соответственно) в момент захвата объекта.

.. image:: controller-interaction-images/image_5.png
    :scale: 80 %
