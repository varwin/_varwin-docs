==========
Varwin SDK
==========

Varwin SDK представляет собой инструмент создания контента (объектов и шаблонов сцен) для платформы Varwin в среде Unity. 

.. important:: Для работы с Varwin SDK необходимо установить платформу разработки Unity версии |unity_version|.


.. toctree::
    :titlesonly:
    :hidden:

    installing/index
    creating/index
    scripting/index
    building/building
    update
    faq