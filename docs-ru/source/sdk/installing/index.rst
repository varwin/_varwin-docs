====================
Установка Varwin SDK
====================

Varwin SDK поставляется в виде файла запакованного проекта Unity (VarwinSDK.unitypackage). Скачайте его.

`Скачать Varwin SDK <http://varw.in/getSDK>`__

.. hint:: Ссылку на скачивание Varwin SDK также можно найти в приложении к письму, полученному в процессе `установки Varwin RMS <https://varwin.readthedocs.io/ru/0.7.0/platform/installing/installing-varwin-platform.html>`__, и на `официальном сайте Varwin <https://varwin.com/>`__.

Для установки SDK: 

1. В Unity |unity_version| откройте Assets - Import Package - Custom Package и выберите скачанный ранее архив с SDK.

.. image:: images/image1.png

2. Откроется окно с выбранными для загрузки элементами. Нажмите Import.

.. image:: images/image4.png

3. Затем откроется окно с рекомендуемыми значениями настроек. Нажмите Accept All.

.. image:: images/image3.png

4. После установки в меню Unity появится раздел Varwin SDK.

.. image:: images/image2.png

.. toctree::
    :titlesonly:
    :hidden:

    settings