====================
Настройка Varwin SDK
====================

В меню *Varwin->Settings* можно найти три пункта:

.. image:: images/settings1.png
    :scale: 80%

SDK
---

В окне *Varwin SDK Settings* можно включить или выключить экспериментальные возможности SDK, такие как сборка объектов с поддержкой Oculus Quest и WebGL.

.. image:: images/settings2.png
    :scale: 80%
    
Default Author Info
-------------------

В окне *Default Author Info* можно задать информацию о создателе объектов, которая будет проставляться объектам по умолчанию: имя, e-mail и ссылка.

.. image:: images/settings3.png
    :scale: 80%

Project Settings
----------------

Этот пункт меню вызывает окно *Varwin Unity Project Settings*, которое проставляет настройки Unity `во время установки SDK`_. Если все настроено корректно, окно будет пустым.

.. _во время установки SDK: index.html

.. image:: images/settings4.png
    :scale: 80%