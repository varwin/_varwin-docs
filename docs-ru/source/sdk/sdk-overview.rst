====================
Varwin SDK: введение 
====================

Что это такое
=============
SDK (software development kit) — набор средств разработки, который позволяет создавать приложения для определённого пакета программ. 
Varwin SDK представляет собой инструмент создания контента (объекты, шаблоны сцен) для платформы Varwin в среде Unity.  Для работы с Varwin SDK необходимо установить платформу разработки Unity версии |unity_version|.

Установка 
=========
Varwin SDK поставляется в виде файла запакованного проекта Unity (VarwinSDK.unitypackage). Файл можно загрузить `по ссылке. <http://varw.in/getSDK>`__

`Как установить Varwin SDK <https://varwin.readthedocs.io/ru/latest/sdk/sdk-install.html>`__

Создание объектов
==================
Создавайте объекты для платформы Varwin в среде Unity. В качестве основы для создания объекта может использоваться любой префаб.

.. image:: images/image10.png
	:scale: 70%
	
.. image:: images/image12.png
	:scale: 70%
	
`Создание объекта для платформы Varwin в Unity: инструкция	<https://varwin.readthedocs.io/ru/latest/sdk/creating-objects.html>`__

Файл объекта
------------
Varwin SDK упаковывает созданный объект в файл с расширением .vwo. Затем файл экспортируется в платформу Varwin.

.. image:: images/image8.png

`Загрузка объектов из Unity: инструкция <https://varwin.readthedocs.io/ru/latest/sdk/uploading-to-library.html>`__


**Что содержит файл:**

.. image:: images/image9.png

Файл bundle содержит модель конкретного объекта (game object) полностью, кроме кода.
bundle.manifest - метаинформация для bundle.
bundle.json - технический файл, содержащий пояснения о содержимом файла bundle, и описание используемого программного кода.
bundle.png - иконка объекта. Отображается в Blockly и в меню на шаблоне сцены (spawnmenu).
install.json - файл для Blockly. Нужен для автоматического создания блоков для объектов (подробнее: `Работа с Blockly - Блоки для объектов <https://varwin.readthedocs.io/ru/latest/manuals/blockly.html#id7>`__).
файлы с расширением .dll - исполняемые фрагменты объекта.

Создание шаблонов сцен
======================
Создавайте шаблоны сцен для платформы Varwin в среде Unity.

`Создание шаблона сцены: инструкция <https://varwin.readthedocs.io/ru/latest/sdk/creating-scene-templates.html>`__

.. image:: images/image2.png
	:scale: 70%
	
.. image:: images/image4.png
	:scale: 70%
	
Файл шаблона сцены
------------------
Varwin SDK упаковывает созданный шаблон сцены в файл с расширением .vwst. Затем файл экспортируется в платформу Varwin.

.. image:: images/image1.png

Импорт моделей
==============
Импортируйте модели FBX, obj, blend и glTF через Unity в платформу Varwin - как по одной, так и папку целиком.

.. image:: images/image5.png

.. image:: images/image6.png

Задавайте имя объекта, можно ли его перетаскивать или использовать, имеет ли он физические свойства, его размеры в метрах и его массу.

Версионирование объектов и шаблонов сцен
========================================
Создавайте новые версии уже созданных объектов и шаблонов сцен.

`Версионирование объектов и шаблонов сцен: инструкция <https://varwin.readthedocs.io/ru/latest/sdk/versioning.html>`__

.. image:: images/image3.png
	:scale: 70%

*Объект*

.. image:: images/image11.png
	:scale: 70%

*Шаблон сцены*

Массовая сборка объектов
========================
Делайте массовую сборку созданных вами объектов для платформы Varwin.

.. image:: images/image7.png











