==============================================
Создание объектов для платформы Varwin в Unity
==============================================
Видеоуроки
==========

Просмотрите видеоуроки по созданию объектов для платформы Varwin RMS.

**Создание объекта Кнопка**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/CHe7glQ-abo" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|
	
**Создание объекта Лампочка**	
	
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/jcZig-coY7k" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>	

|

**Создание объекта Дисплей**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/wuaM8-eNLOc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>	

|

Создание объектов
=================
1. Создайте или импортируйте объект и сохраните его как префаб.
2. Выделите префаб в папке.
3. Откройте VARWIN SDK -> Create -> Object. 

.. image:: images2/image11.png

4. Откроется окно создания объекта. Если префаб был выделен перед открытием окна, он автоматически добавится в поле Game Object. 
5. Дайте объекту желаемое название. Добавьте теги для быстрого поиска объекта. Добавьте информацию об авторе.
6. Нажмите Create. Система предложит немного подождать.

.. image:: images2/image2.png

7. Объект создан. Он состоит из префаба объекта, .asmdef-файла, который нужен для компиляции кода объекта, и класса объекта в уникальном namespace. Если появится необходимость добавить еще классов, они должны будут лежать в той же самой директории и находиться в том же самом namespace.

.. image:: images2/image8.png

8. Для объекта создался класс SimpleButton, наследующий VarwinObject. Это основной скрипт объекта, который соединяет его с платформой и реализует логику.

.. code:: c#

    using UnityEngine;
    using Varwin.Public;
    namespace Varwin.Types.Cube_68bc9ae493d843aab141939fcd529513
    {
        [Locale(SystemLanguage.English,"Cube")]
        [Locale(SystemLanguage.Russian,"Cube")]
        public class Cube : VarwinObject
        {
            void Start()
            {
            }

            void Update()
            {
            }
        }
    }
	
9. Все готово для написания кода объекта.

Пример
------
У объекта в его основном классе есть метод “изменять состояние”, которая изменяет булевую переменную IsPressed:

.. code:: c#

    public void ChangeState()
                  {
                          isPressed = !isPressed;
                          _animator.SetBool("Pressed", isPressed);

- Чтобы связать этот метод с нажатием триггера на контроллере (Use), пробросим метод “ChangeState” в Interactable Object Behavior.

.. image:: images2/image4.png

- Для Blockly потребуется написать создать функцию, прописать ей атрибут (в данном случае Checker) и его название.

Подробнее: `Работа с атрибутами <https://varwin.readthedocs.io/ru/latest/sdk/attributes.html>`__

.. code:: c#

    [Checker("buttonState")]
    [Locale(SystemLanguage.English, "button pressed")]
    public bool ButtonPressedChecker()
    {
          return isPressed;
    }
	
- Если у объекта будет несколько методов, то Blockly сгруппирует их в единый блок внутри этого названия.

Например, можно написать код для состояния кнопки “отпущена”.

.. code:: c#

     [Checker("pressed")]
        [Locale(SystemLanguage.English, "is released")]
        public bool IsReleased()
        {
              return !IsPressed;
        }

- Тогда Blockly сгруппирует методы таким образом:
   
.. image:: images2/image1.png  

*Пример группировки блоков*

- Локализация

.. code:: c#

     [Checker("buttonState")]
     [Locale(SystemLanguage.English, "button pressed")] 
     [Locale(SystemLanguage.Russian, "кнопка нажата")]
     public bool ButtonPressedChecker()
     {
      return isPressed;
     }

*Локализация команд*

.. code:: c#

     namespace Varwin.Types.TutorialButton_eb5da3802fc64af68235a9d8de7987df
	  {
		  [Locale(SystemLanguage.English,"simple button")]
          [Locale(SystemLanguage.Russian,"простая кнопка")]
          public class TutorialButton : VarwinObject


*Локализация названия объекта*

- Все готово для построения объекта. Нажмите Build. 
- Когда объект будет готов, откроется содержащая его папка. Загрузите готовый объект в библиотеку Varwin.

.. image:: images2/image14.png 

- Объект появился в библиотеке

.. image:: images2/image10.png

- Разместите объект в сцене в виртуальной реальности. Нажмите “Сохранить”.
- Объект появился в сцене. Откройте Blockly. Вы увидите логические блоки, созданные для объекта.

.. image:: images2/image13.png

.. image:: images2/image6.png
   :scale: 80%

Работа с атрибутами
-------------------

Возможности работы с атрибутами при создании объектов описаны здесь: `Работа с атрибутами <https://varwin.readthedocs.io/ru/latest/sdk/attributes.html>`__

Версионирование объектов
------------------------

При необходимости вы можете создать новую версию вашего объекта. 
`Версионирование объектов и шаблонов сцен: инструкция <https://varwin.readthedocs.io/ru/latest/sdk/versioning.html>`__

Дополнительные материалы
------------------------
`Игровые объекты в Unity <https://docs.unity3d.com/ru/530/Manual/GameObjects.html>`__



























