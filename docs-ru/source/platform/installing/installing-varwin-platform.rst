==========================
Установка платформы Varwin
==========================

1. Зайдите в раздел `Pricing (Тарифы) <https://varwin.com/pricing/>`__ официального сайта Varwin. Выберите тариф.

2. Вы увидите форму регистрации. Заполните все поля.

.. hint:: Форма регистрации доступна на русском языке.

3. Проверьте почту. Вам должно прийти письмо со ссылкой на скачивание установочной программы Varwin и лицензионным ключом. 

.. hint:: Если вы не получили письмо, проверьте папку Спам.

К письму будет приложена `ссылка <http://varw.in/getSDK>`__ на скачивание файла с архивом для установки Varwin SDK. При необходимости скачайте его.

`Подробнее:` `Инструкция по установке SDK <https://varwin.readthedocs.io/ru/0.7.0/sdk/installing/index.html>`__   

4. Скачайте установочную программу Varwin по ссылке из письма. Это может занять несколько минут.

5. Выберите папку для установки платформы Varwin. Следуя дальнейшим инструкциям установщика, установите программу. Это может занять несколько минут. Не забудьте выбрать опцию автозапуска, если хотите, чтобы Varwin запускался при включении ПК.

.. image:: images1/image1.png

6. После завершения установки значок Varwin появится в области уведомлений в правом нижнем углу экрана. Пока программа запускается, на значке будет желтая точка. Когда программа будет готова к работе, желтая точка исчезнет. 

.. image:: images1/image22.png
	:scale: 75%