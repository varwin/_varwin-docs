===================================
Установка и запуск платформы Varwin
===================================

Подготовка к установке платформы 
================================

1. Проверьте соответствие вашего ПК `системным требованиям. <https://varwin.readthedocs.io/ru/0.7.0/platform/installing/system-requirements.html#id2>`__
2. Для работы с платформой Varwin на вашем ПК должны быть установлены следующие программы:

- Steam
- SteamVR
- ПО для оборудования VR

.. toctree::
    :titlesonly:
    :hidden:
    
    system-requirements
    installing-steam-vr
    installing-vr-headset-software
    installing-varwin-platform