=============================================
Роли и функции пользователей платформы Varwin
=============================================

Администратор 
=============

*(в настоящее время эту функцию может выполнять любой пользователь)*

- Устанавливает и запускает платформу Varwin

Инструкции и сопутствующие документы
------------------------------------

`Установка и запуск платформы <https://varwin.readthedocs.io/ru/latest/manuals/install.html>`__

`Оборудование и системные требования для платформы Varwin <https://varwin.readthedocs.io/ru/latest/platform/equipment.html>`__

`Роли пользователей платформы Varwin <https://varwin.readthedocs.io/ru/latest/platform/roles.html>`__

`FAQ <https://varwin.readthedocs.io/ru/latest/platform/faq.html>`__

Создатель объектов, шаблонов сцен 
=================================

*(скорее всего, это Unity-разработчик)*

- Создает объекты и шаблоны сцен из готовых моделей в среде Unity
- Пишет для них логику
- Реализует для объектов блоки в Blockly

Инструкции и сопутствующие документы
------------------------------------

`Varwin SDK <https://varwin.readthedocs.io/ru/latest/sdk/sdk-overview.html>`__

`Установка Varwin SDK <https://varwin.readthedocs.io/ru/latest/sdk/sdk-install.html>`__

`Создание объектов для платформы Varwin в Unity <https://varwin.readthedocs.io/ru/latest/sdk/creating-objects.html>`__

`Создание шаблонов сцен для платформы Varwin в Unity <https://varwin.readthedocs.io/ru/latest/sdk/creating-scene-templates.html>`__

`Загрузка контента в библиотеку платформы Varwin из Unity <https://varwin.readthedocs.io/ru/latest/sdk/uploading-to-library.html>`__

`Создание библиотек с общим кодом <https://varwin.readthedocs.io/ru/latest/sdk/creating-libraries.html>`__

`Версионирование объектов и шаблонов сцен <https://varwin.readthedocs.io/ru/latest/sdk/versioning.html>`__

`Работа с атрибутами <https://varwin.readthedocs.io/ru/latest/sdk/attributes.html>`__

`Публичные интерфейсы <https://varwin.readthedocs.io/ru/latest/sdk/public-interfaces.html>`__

`FAQ <https://varwin.readthedocs.io/ru/latest/platform/faq.html>`__

Создатель проекта
=================

- Загружает новые объекты и шаблоны сцен в библиотеку
- Создает и редактирует проекты
- Используя объекты и шаблоны сцен из библиотеки и блоки Blockly, пишет сценарий
- Формирует требования к объектам и блокам Blockly, необходимым для разработки (определяет необходимые для сценария блоки и логику поведения объекта) - либо выбирает из готовых

Инструкции и сопутствующие документы
------------------------------------

`Платформа Varwin - общая информация <https://varwin.readthedocs.io/ru/latest/platform/general.html>`__

`Установка Varwin <https://varwin.readthedocs.io/ru/latest/manuals/install.html>`__

`Приложение Varwin RMS <https://varwin.readthedocs.io/ru/latest/platform/rms.html>`__

`Использование VR-контроллеров <https://varwin.readthedocs.io/ru/latest/manuals/controllers.html>`__

`Создание VR-проекта <https://varwin.readthedocs.io/ru/latest/manuals/creating-vr-projects.html>`__

`Работа с Blockly <https://varwin.readthedocs.io/ru/latest/manuals/blockly.html>`__

`FAQ <https://varwin.readthedocs.io/ru/latest/platform/faq.html>`__

Обучение в Varwin RMS

.. image:: media/image27.png

Конечный пользователь
=====================

- Запускает готовое приложение (из интерфейса Varwin или из файла .exe)
- Проходит сценарий

Инструкции и сопутствующие документы
------------------------------------

`Оборудование и системные требования для платформы Varwin <https://varwin.readthedocs.io/ru/latest/platform/equipment.html#>`__

`Использование VR-контроллеров <https://varwin.readthedocs.io/ru/latest/manuals/controllers.html>`__

`Приложение Varwin RMS <https://varwin.readthedocs.io/ru/latest/platform/rms.html>`__

`FAQ <https://varwin.readthedocs.io/ru/latest/platform/faq.html>`__


