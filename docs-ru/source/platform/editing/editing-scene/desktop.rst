====================
Редактирование на ПК
====================

Откройте проект. Напротив сцены нажмите "Редактировать на ПК”. Откроется редактор сцены.

.. image:: desktop-images/image_8.png

Управление
----------

**Панель инструментов**

.. image:: desktop-images/image_0.png

0. дублировать объект - ctrl + d
1. сохранить - ctrl + s
2. отменить действие - ctrl + z
3. повторить действие - ctrl + y
4. копировать - ctrl + c
5. вставить - ctrl + v
6. перемещать камеру - Q
7. перемещать объект - W
8. вращать объект - E
9. масштабировать объект - R

**Переключатель оси**

.. image:: desktop-images/image_1.png
	:scale: 80%

Когда переключатель находится в положении “Локальная”, объект вращается и перемещается относительно собственной оси. Когда переключатель в положении “Мировая”, объект вращается и перемещается относительно осей xyz в системе координат.

**Переключатель  точки**

.. image:: desktop-images/image_2.png

Когда переключатель находится в положении “Точка опоры”, объект вращается и перемещается относительно определенной точки, заданной разработчиком. Когда переключатель в положении “Центр”, объект вращается и перемещается относительно своего геометрического центра.

**Переключатель перспективы**

.. image:: desktop-images/image_3.png

Переключатель регулирует переход из стандартной перспективы в ортографическую проекцию.

**Переход в режим предпросмотра**

.. image:: desktop-images/image_4.png

1. VR
2. ПК*

\*\ Чтобы выйти из режима просмотра на ПК, нажмите клавишу Escape. После нажатия будет предложен выбор: переключиться в VR, вернуться в режим редактирования на ПК или остаться в режиме просмотра (Отмена).

.. image:: desktop-images/image_5.png
	:scale: 55%

**Библиотека и объекты**

Перемещайтесь между библиотекой объектов и списком размещенных на сцене объектов. Здесь же расположена строка поиска. 
Выделив объект в списке размещенных объектов, вы тем самым выделяете его на сцене.

.. image:: desktop-images/image_6.png

**Информация об объекте**

В окне информации в правой части экрана отображается название, ID и тип выделенного объекта и цифровые параметры его положения в пространстве. Также здесь можно удалить выделенный объект.

.. image:: desktop-images/image_7.png