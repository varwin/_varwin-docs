==========================
Проекты со множеством сцен
==========================

Часто в проектах присутствуют более одной сцены. В связи с чем возникает необходимость переключаться между сценами.

Выбор стартовой сцены
=====================

Создайте две сцены в проекте. 

На странице проекта перейдите во вкладку "Конфигурации запуска". Нажмите на иконку карандаша, откроется окно редактирования конфигурации.

.. image:: multi-scene-images/image_0.png

Выберите стартовую сцену.

.. image:: multi-scene-images/image_1.png

.. note:: Проект может иметь множество конфигураций.

Переключение между сценами
==========================

Переключение между сценами настраивается в blockly.

Для переключения между сценами используйте блок "Загрузить сцену", расположенный в разделе "Действия".

Перейдите в режим редактирования стартовой сцены. Добавьте на сцену простую кнопку.

Перейдите в редактор blockly стартовой сцены. Добавьте событие на нажатие кнопки, к событию добавьте блок "Загрузить сцену", выберите вторую сцену. Сохраните изменения. 

.. image:: multi-scene-images/image_2.png
    :scale: 80 %

Для тестирования перейдите в режим просмотра проекта.

.. image:: multi-scene-images/image_3.png

.. warning:: Режим предпросмотра запускает только текущую сцену.

Запустится сцена, выбранная стартовой. Нажмите на кнопку - запустится вторая сцена.
