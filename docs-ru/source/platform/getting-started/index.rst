==============
Начиная работу
==============

Данный раздел рекомендован к изучению пользователям, начинающим знакомство с платформой Varwin RMS.

.. toctree::
    :titlesonly:
    :hidden:

    starting-first-time/index
    vr-controllers-usage/index