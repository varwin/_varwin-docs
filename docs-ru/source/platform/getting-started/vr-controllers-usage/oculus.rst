==================
Контроллеры Oculus
==================

Захват предмета
---------------

Нажмите кнопку захвата.

Чтобы перенести захваченный предмет:

- Режим просмотра: держите нажатой кнопку захвата.
- Режим создателя: не нужно держать кнопку нажатой.
   
Чтобы поставить предмет:

- Режим просмотра: отпустите кнопку захвата.
- Режим создателя: нажмите кнопку “Спусковой крючок”.

.. image:: images/image13.png 
	:scale: 45%

Действие
--------

Нажмите кнопку “Спусковой крючок” (“Курок”, “Триггер”).

.. image:: images/image11.png
	:scale: 40%

Телепортация
------------

1. Нажмите стик. Появится луч-указка.
2. Не отпуская, наведите луч на область, куда требуется телепортация. Если луч зеленого цвета - телепортация возможна.
3. Для телепортации отпустите стик.

.. image:: images/image12.png
	:scale: 45%
	
Поворот
-------
Нажмите стик в левую сторону для дискретного поворота влево, в правую сторону - для поворота вправо

.. image:: images/image12.png
	:scale: 45%

Вызов меню
----------
Меню вызывает кнопка Y (**левый** контроллер)

.. image:: images/image14.png
	:scale: 45%

Также 
-----

- Кнопка “Система” вызывает системное меню Oculus (не связано с платформой Varwin).
- При случайном нажатии - нажмите второй раз, чтобы вернуться.

.. image:: images/image15.png
	:scale: 45%

