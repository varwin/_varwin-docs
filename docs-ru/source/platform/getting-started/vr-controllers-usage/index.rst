=============================
Использование VR контроллеров
=============================

Для работы с платформой можно использовать следующие контроллеры:

HTC Vive
--------

.. image:: images/image2.png
	:scale: 80 %

Oculus
------

.. image:: images/image4.png

Windows Mixed Reality (WMR)
---------------------------

.. image:: images/image3.png

.. toctree::
    :titlesonly:
    :hidden:

    vive
    oculus
    wmr