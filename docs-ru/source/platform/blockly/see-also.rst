========================
Дополнительные материалы
========================

Познакомьтесь с принципами работы Blockly, пройдя `обучение в игровой форме. <https://blockly-games.appspot.com/?lang=ru>`__

Просмотрите `русскоязычный учебник Blockly. <http://blockly.ru/manual/beginning.html>`__

`Другие игры <http://blockly.ru/games.html>`__

`Материалы о Blockly на русском языке. <http://blockly.ru/information.html>`__

`Материалы о Blockly на английском языке. <https://developers.google.com/blockly/guides/overview>`__