=======
Blockly
=======

**Blockly** – визуальный язык программирования, не требующий написания кода. Он позволяет создавать программы, соединяя визуальные блоки друг с другом в соответствии с их формой.

Язык Blockly используется для построения логики сцены - сценария -  при работе с платформой Varwin. Это позволяет снизить порог освоения платформы и открыть ее возможности для пользователей без навыков программирования. 

.. image:: images/image1.png

Если блоки подходят друг к другу, то программа будет работать. Однако возможны логические ошибки, которые обнаружатся в процессе исполнения. При этом блок, который вызвал ошибку, подсвечивается.

Кроме того, в процессе выполнения программы в режиме просмотра подсвечиваются выполняемые в данный момент блоки. Это облегчает наблюдение за ходом логики в реальном времени.

Новичкам мы предлагаем изучить `дополнительные материалы <https://varwin.readthedocs.io/ru/0.7.0/platform/blockly/see-also.html>`__, а также ознакомиться с `интерфейсом редактора Blockly <https://varwin.readthedocs.io/ru/0.7.0/platform/blockly/interface.html>`__ в платформе Varwin.

.. toctree::
    :titlesonly:
    :hidden:

    interface
    blocks
    execution-order
    see-also