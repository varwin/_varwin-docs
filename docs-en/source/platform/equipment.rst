=========================================================
Equipment and System Requirements for the Varwin Platform
=========================================================

The Varwin platform requires: 

- VR equipment set
- personal computer fitting the following system requirements.

VR equipment
============

1. VR equipment set (options a-c at choice):

 **a. HTC Vive** 
 
 - full kit: VR headset - 1 pcs, wireless controllers - 2 pcs, base stations - 2 pcs., accessories
 - photo stands - might be useful for base stations placement, depending on room characteristics and desired arrangement of the base stations.
	  
 **b. Oculus** 
 
 - VR headset - 1 pcs, wireless controllers - 2 pcs, Oculus sensors - 2 pcs, accessories
 
 **c. Windows Mixed Reality (WMR) (any manufacturer)**
 
 - VR headset - 1 pcs, wireless controllers - 2 pcs, accessories
 - WMR kit doesn’t require any additional movement trackers
 
.. hint:: The `Desktop Editor <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#editing-without-vr-desktop-editor>`__ and `Desktop Player <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#preview-without-vr-desktop-player>`__ features allow to create VR projects on the Varwin platform even without VR equipment.  

PC system requirements
======================

1. OS: Windows 10 and higher, x64    
2. Processor: Intel Core i5 - i7* or AMD Ryzen 5
3. Memory: 8 Gb and higher
4. Graphics card: GeForce GTX 1060 6 Gb video memory and higher* (or similar in performance)  
5. 10+ Gb hard drive free space
6. USB 3.0 motherboard and power supply unit, corresponding with the above specifications
7. **(for WMR)**: Bluetooth 4.0
8. Video output: HDMI 1.4, 2.0 or DisplayPort 1.3 (depending on the chosen equipment requirements)

\*\ depending on content complexity

Compatibility check
===================

VR equipment manufacturers offer compatibility check tools for Windows-based PCs. Choose equipment of interest and download compatibility check tool from official sources.

- `HTC Vive compatibility check <http://dl4.htc.com/vive/ViveCheck/ViveCheck.exe>`__
- `Windows Mixed Reality compatibility check <https://www.microsoft.com/en-us/store/p/windows-mixed-reality-pc-check/9nzvl19n7cnc?rtc=1>`__
- `Oculus Rift compatibility check <https://ocul.us/compat-tool>`__

Software
========

The following software must be installed  on your PC in order for the Varwin system to operate:

1. Steam
2. Steam VR
3. **(for Windows Mixed Reality equipment)**: Windows Mixed Reality for SteamVR
4. `Unity <https://unity3d.com/get-unity/download/archive>`_ |unity_version| (for creating objects and/or scene templates)

For detailed instructions on required software installation, please see `Varwin installation and launch - Preparing your PC. <https://varwin.readthedocs.io/en/latest/manuals/install.html#preparing-your-pc>`__
