=======================================
Roles and Functions of the Varwin Users
=======================================

Administrator
=============

*(currently, this function can be carried out by any user)*

- Installs and launches Varwin platform

Instructions and related documents
----------------------------------

`Varwin installation and launch <https://varwin.readthedocs.io/en/latest/manuals/install.html>`__

`Equipment and system requirements for Varwin platform <https://varwin.readthedocs.io/en/latest/platform/equipment.html>`__

`Roles and functions of Varwin users <https://varwin.readthedocs.io/en/latest/platform/roles.html>`__

`FAQ <https://varwin.readthedocs.io/en/latest/faq/faq.html>`__

Creator of objects & scene templates 
====================================

*(most likely, a Unity programmer)*

- Creates objects and scene templates out of ready-made prefabs in Unity
- Writes logic of their interaction
- Makes Blockly blocks for the objects

Instructions and related documents
----------------------------------

`Varwin SDK <https://varwin.readthedocs.io/en/latest/sdk/sdk-overview.html>`__

`Installing the Varwin SDK <https://varwin.readthedocs.io/en/latest/sdk/sdk-install.html>`__

`Creating objects for Varwin in Unity <https://varwin.readthedocs.io/en/latest/sdk/creating-objects.html>`__

`Creating scene templates for Varwin in Unity <https://varwin.readthedocs.io/en/latest/sdk/creating-scene-templates.html>`__

`Uploading content into Varwin library from Unity <https://varwin.readthedocs.io/en/latest/sdk/uploading-to-library.html>`__

`Creating libraries with shared code <https://varwin.readthedocs.io/en/latest/sdk/creating-libraries.html>`__

`Objects, scene templates versioning <https://varwin.readthedocs.io/en/latest/sdk/versioning.html>`__

`Working with attributes <https://varwin.readthedocs.io/en/latest/sdk/attributes.html>`__

`Public interfaces <https://varwin.readthedocs.io/en/latest/sdk/public-interfaces.html>`__

`FAQ <https://varwin.readthedocs.io/en/latest/faq/faq.html>`__

Project creator
===============

- Uploads new objects and scene templates to the library
- Creates and edits projects
- Creates project scenario using objects and scene templates from the library and blocks in Blockly
- Shapes up requirements for the new objects and Blockly blocks, or selects from ready ones.

Instructions and related documents
----------------------------------

`Varwin platform: general information <https://varwin.readthedocs.io/en/latest/platform/general.html>`__

`Varwin RMS App <https://varwin.readthedocs.io/en/latest/platform/rms.html>`__

`Using VR controllers <https://varwin.readthedocs.io/en/latest/manuals/controllers.html>`__

`Creating VR projects <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html>`__

`Working with Blockly <https://varwin.readthedocs.io/en/latest/manuals/blockly.html>`__

`FAQ <https://varwin.readthedocs.io/en/latest/faq/faq.html>`__

Varwin RMS tutorial

.. image:: media/image26.png

End-user
========

- Launches end product (an app built out of VR project) from Varwin interface or from .exe file
- Interacts with the end product

Instructions and related documents
----------------------------------

`Equipment and system requirements for Varwin platform <https://varwin.readthedocs.io/en/latest/platform/equipment.html>`__

`Using VR controllers <https://varwin.readthedocs.io/en/latest/manuals/controllers.html>`__

`Varwin RMS App <https://varwin.readthedocs.io/en/latest/platform/rms.html>`__

`FAQ <https://varwin.readthedocs.io/en/latest/faq/faq.html>`__


