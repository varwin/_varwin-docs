==============
Varwin RMS App
==============

Varwin app is the environment for creating and editing VR projects. 
Varwin app is installed on computers of everyone participating in a project.

Video Guides
============

Here you will find the introductory video guides showing an overview of the Varwin RMS platform and of its sections. 

**Introduction to the Varwin RMS platform**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/F2esGCQ2_No" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

**The Object section of the Varwin RMS platform**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/g9gEya5S6sc" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

**The Scene Template section of the Varwin RMS platform**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/lI0G9Lc1Sug" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

**The Project section of the Varwin RMS platform**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/DxKxSBuWoaE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

App modes
=========

The Varwin RMS app works in Edit mode, Preview mode and View mode.

Edit mode
---------

Editing in VR
^^^^^^^^^^^^^

- Edit mode in VR is used to spawn objects on the scene
- In Edit Mode, physical laws do not affect objects. They are applied in Preview and View modes.

.. image:: media/image21.png

Editing without VR: Desktop Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use your mouse and keyboard to edit VR scenes. The Desktop Editor feature allows editing without VR, namely place objects on the scene and set their positions. To use this feature, click “Edit on desktop.”

.. image:: media/image7.png

*Learn more:* `Editing without VR (Desktop Editor) <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#editing-without-vr-desktop-editor>`__

Creating the project scenario
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Project scenario and logic are created in Blockly visual editor. No programming skills are required. 

.. image:: media/image4.png

.. image:: media/image16.png
   :scale: 70%

*Learn more:* `Working with Blockly <https://varwin.readthedocs.io/en/latest/manuals/blockly.html>`__

- All necessary logic blocks are provided 
- Users can create and name necessary variables

.. image:: media/image24.png
   :scale: 70%

*Pre-provided logic blocks*

.. image:: media/image18.png
   :scale: 70%

*Variables creation option; custom variables*

Code editing
^^^^^^^^^^^^

If necessary, the user can edit the code of the project.

.. image:: media/image12.png

Preview mode
------------

- In Preview mode, you can evaluate each scene of your future project separately.
- The Preview mode works both in VR or in Desktop Editor. 
- Physical laws are applied to objects*. 
- The objects engage in the preset logic (e.g., a display shows specific text when specific conditions are met.) 

\*\ Those objects which have been assigned relevant parameter when created

Scene preview in VR
^^^^^^^^^^^^^^^^^^^

.. image:: media/image19.png

Scene preview on desktop (Desktop Player)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: media/image23.png

*Learn more:* `Preview without VR (Desktop Player) <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#preview-without-vr-desktop-player>`__

Project view mode
-----------------

View mode allows the user to evaluate the whole project before building it. You can view your project either on desktop or in VR.

.. image:: media/image2.png

View mode can be switched on if the project has at least one configuration.

.. image:: media/image22.png

*Learn more:* `App launch configurations <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#app-launch-configurations>`__

- Physical laws are applied to all objects.
- View mode doesn’t allow to switch to Edit mode, nor to make any editing. 
- Interaction with configuration (ready-made virtual world) following a preset script.

*See also:* `VR menu functions <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#vr-menu-functions>`__

Library
=======
Varwin app provides the user with a library storing the following:

- project settings,
- project metadata,
- scene templates to create scenarios in,
- objects to set in the scene templates.

Varwin allows users to create new objects and scene templates and upload them to the library. This will require Varwin SDK - development tools tailored for Varwin platform and provided in the packaged solution.

*Learn more:* `Varwin SDK <https://varwin.readthedocs.io/en/latest/sdk/sdk-overview.html>`__

Project structures
------------------

.. image:: media/image13.png

*Projects metadata in the library*

Project metadata is a blank VR project for further editing. It contains IDs of scene templates and objects, coordinates of objects’ placement, the logic for Blockly. This file is editable.

Scene templates and objects
---------------------------

Scene templates and objects are stored in the relevant sections of the Library. Both their names and icons are displayed in the list. 

.. image:: media/image11.png

*Scene templates in the library*

.. image:: media/image8.png

*Objects in the library*

.. image:: media/image20.png

*Object menu in VR*

Versioning of scene templates and objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An object/scene template can have different versions. An RMS user can view versions of an object/scene template by clicking the Default Version button in the RMS.

.. image:: media/image14.png

- The latest version of an object/scene template is set by the system as a default one. 

.. image:: media/image10.png
   :scale: 80%

- If a user selects another version of an object/scene template as a default one, a sign of a lock appears on the Default Version button. It means that a user default version is selected for this object/scene template. 

.. image:: media/image15.png

.. image:: media/image3.png
   :scale: 75%

- This choice is fixed for all the projects. 
- If an RMS user uses a version of an object/scene template in a project and then changes the default version for this object/scene template in the library, the change won’t affect that project.
- There can be any number of versions to choose from.

Tags
----

Tags can be assigned to objects and/or scene templates in order to simplify their finding and systematizing.

.. image:: media/image9.png

*Tags in the library*

- The tags assigned in the library will be displayed in VR. 

.. image:: media/image25.png

*Tags in VR*

- Tags are editable
- For tag list in VR, click Filter. The digit icon on this button displays the number of tags applied. 

.. image:: media/image6.png

*Tag list in VR*

Files for export/import 
-----------------------

Files containing objects, scene templates, project metadata can be imported into the system. 
Project metadata and ready apps can be exported as files. Scene templates and objects are not exportable.

.. image:: media/image17.png

*Import of objects, scene templates, project metadata*

.. image:: media/image1.png

*Export of project metadata*

End product
===========

When the project is completed, an app for the end user can be built out of it. 

- The apps are exported as .exe files. They can be launched on any PC. 
- Varwin platform installation is not needed to launch an exported app. 
- The apps are not editable.
- Not available for Starter edition. 

.. image:: media/image5.png

*Build a ready app (not available for Starter edition)*

