=====================
Working with Blockly
=====================

Introduction to Blockly
=======================

Blockly is a visual code editor that does not require programming skills. It allows writing program scripts by connecting visual blocks. The blocks are divided into groups according to their purpose or return value. Each group has its own colour. The shapes of the blocks also vary; not all the blocks are compatible.
If blocks fit together, the program will run. Still, logical errors can arise in the process. The block causing the error will be illuminated.
Also, when the program runs in preview mode the blocks which are executed at the moment are illuminated as well. This simplifies watching the script in real-time. 

.. image:: media3/image21.png

*Illuminated block (is executed at the moment)*

The blocks are managed (copied, deactivated, removed, etc.) by right-clicking on them and keyboard shortcuts Ctrl+C, Ctrl+V.
The blocks can be expanded and collapsed by right-clicking on them.
Blockly interface consists of Workspace, where logic algorithms of the project are built, and Toolbox - side menu from whence the user may select blocks or create new ones.

.. image:: media3/image18.png
   :scale: 75%

Blockly is used for composing scripts for VR projects powered by the Varwin platform. This opens access to our platform to users with no programmer skills. 

Further reading
---------------
Get acquainted with Blockly

`Introduction to Blockly (official site) <https://developers.google.com/blockly/guides/overview>`__

`Training games <https://blockly-games.appspot.com/?lang=en>`__

Standard sections
=================

Training games have acquainted you with the standard block sections. These are used within Varwin platform:

**- Logic**

Main logic blocks. 

.. image:: media3/image14.png
   :scale: 75%

**- Variables**

A variable is an area of memory where to store a certain value to refer to it later. Strictly speaking, an object in itself is a variable.

.. image:: media3/image6.png
   :scale: 75%

**- Lists**

Set of blocks allowing to work with lists. A list is a set of variables of any type. 

.. image:: media3/image1.png
   :scale: 75%

**- Loops**

Blocks allowing to set a number of actions with foregone iteration count. Can be used also to avoid lists. 

.. image:: media3/image15.png
   :scale: 75%
   
**- Math**

Set of blocks allowing to work with numbers.

.. image:: media3/image4.png
   :scale: 75%

**- Text**

Set of blocks allowing to work with texts.

.. image:: media3/image10.png
   :scale: 75%

**- Functions**

Set of blocks allowing to create and call functions. Functions are good for structuring and multiple usage of repeatable logic.

.. image:: media3/image20.png
   :scale: 75%

Custom sections
===============

These sections have been created specially for the Varwin platform.

Actions
-------

Commands which invoke certain system actions.

.. image:: media3/image24.png
   :scale: 60%

Events
-------

All the blocks in the workspace are executed on the frame to frame basis. The exceptions are blocks from Function and Event groups. 
Blocks placed inside an event are executed only at the moment of an event.
Events section stores general system events; e.g., **on init** (at the moment of initialization) event.

.. image:: media3/image17.png
   :scale: 75%

Blocks that are inside this event are operated only once, at the moment of scene load or of changing the mode to Preview.
This block is good for the initialization of objects state within a scene, e.g., switching on a button which is switched off by default. The block can be used multiple times. 

**Example**

.. image:: media3/image5.png

- сommands 1, 2 are executed only once at the moment of scene load or of changing the mode to Preview, 
- command 3 is executed frame by frame, 
- command 4 is executed at the moment of event (pressing on the panel.)

Objects
-------
Objects spawned on the scene in VR are displayed in this section. Available states (variables) for each object are shown as well (ill. 1)
- **object:any** is a section with blocks that can be applied to any object.
- Objects added to a scene are grouped by type (ill.2: area, button, display, etc.) The specific object can be selected from the drop-down list (ill.3)

.. image:: media3/image7.png
   :scale: 85%

*ill.1*

.. image:: media3/image8.png
   :scale: 90%

*ill.2*

.. image:: media3/image3.png
   :scale: 80%

*ill.3*

Blocks for objects
~~~~~~~~~~~~~~~~~~~

+-------------+-----------+-------------------------+------------------------------+
|**Section**  |**Name**   |**Purpose**              |**Picture**                   |
+=============+===========+=========================+==============================+
|Logic        |   checker |  Check true/false       |.. image:: media3/image2.png  |
+-------------+-----------+-------------------------+------------------------------+
|Action       |   action  | Actions with object     |.. image:: media3/image13.png |
+-------------+-----------+-------------------------+------------------------------+
|Variables    |   getter  |  Get a state            |.. image:: media3/image22.png |
+             +-----------+-------------------------+------------------------------+
|             |   setter  |  Set a state            |.. image:: media3/image23.png |
+-------------+-----------+-------------------------+------------------------------+
|Events       |   event   |  Events with object     |.. image:: media3/image12.png |
+-------------+-----------+-------------------------+------------------------------+

Creating custom blocks
~~~~~~~~~~~~~~~~~~~~~~
Custom blocks can be created along with object code. See an example here: `Creating objects for Varwin in Unity - Example <https://varwin.readthedocs.io/en/latest/sdk/creating-objects.html#example>`__

Further reading
---------------
`Custom blocks (official site) <https://developers.google.com/blockly/guides/configure/web/custom-blocks>`_

How Blockly works on Varwin platform
====================================

To open Blockly, 

- Open relevant project,
- Click “Open visual logic editor”.

.. image:: media3/image19.png

You may proceed to creating algorithms.

.. image:: media3/image11.png
   :scale: 60%

The new algorithm can be immediately checked in VR. To do it, click ‘Run in VR.’ 

.. image:: media3/image16.png

Logic set in the script is executed in VR frame by frame, except Function and Event blocks.




