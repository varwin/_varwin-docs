==============================
Varwin installation and launch
==============================

Preparing your PC
==================

1. Check if your PC complies with our `system requirements. <https://varwin.readthedocs.io/en/latest/platform/equipment.html#pc-system-requirements>`__
2. The following software must be installed on your PC in order for the Varwin system to operate:
   
   a. Steam
   b. SteamVR
   c. Unity |unity_version| (for creating objects and/or scene templates)

Steam installation
-------------------

Download and run `Steam installer <https://steamcdn-a.akamaihd.net/client/installer/SteamSetup.exe>`__ following this link from `official source. <https://store.steampowered.com>`__
Enter your Steam account. If you don’t have one, please register. 

.. image:: media1/image9.png
	:scale: 65%
	
SteamVR installation
--------------------

1. Open Steam installed on your PC. Go to the Store section and search for SteamVR (no space between the words.)

   a. Check if your PC complies with SteamVR requirements (run SteamVR Performance Test) if necessary. 
   b. Download SteamVR. 
   
.. image:: media1/image11.png

2. When SteamVR is downloaded, you’ll be notified that it is in your library. Click “Play Now” in order to install SteamVR.

 .. image:: media1/image2.png
 
3. Follow the instructions of the installer to install SteamVR.

 .. image:: media1/image3.png
 	:scale: 85%
 
For WMR equipment: Windows Mixed Reality for SteamVR installation
-----------------------------------------------------------------
Those using Windows Mixed Reality equipment will have to install one more program: Windows Mixed Reality for SteamVR. The installation process is equivalent to that of SteamVR.

1. Open Steam installed on your PC. Go to the Store section and search for Windows Mixed Reality for SteamVR. Download this program.

 .. image:: media1/image15.png
 
2. When the program is downloaded, you’ll see a notification that you can now use it. Click “Free” in order to install Windows Mixed Reality for SteamVR.

 .. image:: media1/image8.png
 
3. Follow the instructions of the installer to install Windows Mixed Reality for SteamVR.

 .. image:: media1/image3.png
 	:scale: 85%
 
For Oculus equipment: Oculus software installation
---------------------------------------------------
Those using Oculus equipment will have to install one more program: Oculus software. Download it from official Oculus site using this link: `Get Started with Rift. <https://www.oculus.com/rift/setup/>`__

 .. image:: media1/image6.png
 	:scale: 75%
 
Unity installation
-------------------

Follow this `link from official source <https://unity3d.com/get-unity/download/archive?_ga=2.19733694.707766669.1553265932-456492208.1549287338>`__ to install Unity |unity_version|.
 
Varwin platform installation
============================
1. Go to `Varwin official website <https://varwin.com/>`__. Click “Prices” and select one.
2. You’ll see the registration form. Please fill it in (all fields are required.)

 .. image:: media1/image111.png
 
3. Check your email. You must have received a letter containing links for downloading the Varwin installer and a license key. If you haven’t received the letter, please check your Spam folder. 

   a. This email will include the `link <http://varw.in/getSDK>`__ for downloading Varwin SDK. If you need to install the Varwin SDK, please follow the link to download the file. 
   
   `How to install the the Varwin SDK. <https://varwin.readthedocs.io/en/latest/sdk/sdk-install.html>`__
 
4. Follow the link you’ve received to download the Varwin installer. This may take several minutes.
5. Follow the instructions of the installer to install the Varwin platform. This may take several minutes. 

 .. image:: media1/image1.png
 
6. When Varwin is installed, its icon will appear in the notification area in the bottom right corner of the screen. While the program is launching, there will be a yellow dot on the icon. When the program is work-ready, the dot will disappear. 

 .. image:: media1/image222.png
 	:scale: 75%
 
Launching the platform
======================

1. Launch Varwin by double-clicking on the icon on your desktop, or by double-clicking on the icon in the notification area, or via the menu. 
   
   a. When Varwin is installed, you can open the menu by right- or left-clicking on the icon in the notification area

 .. image:: media1/image16.png
 
2. At the first launch, you will get several notifications from Windows Firewall. Select “Allow access” (by doing this, you allow access to certain Varwin services, without which the platform won’t run.)

 .. image:: media1/image7.png
 
3. You’ll see a license key window. Enter the key you’ve received in the letter.

 .. image:: media1/image5.png 
 	:scale: 55%
 
4. You have successfully installed and launched Varwin.