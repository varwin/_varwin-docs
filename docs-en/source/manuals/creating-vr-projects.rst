=====================
Creating VR Projects
=====================

What this is
============

Creating a VR project means building a simulated environment from ready-made models. The resulting environment will contain required objects and will function by pre-defined script.

Video Guide
===========

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/qyCwzufT5Fk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

Creating a project, adding scenes
=================================

1. Run the Varwin RMS app. 
2. Enter the Project List section. Click ‘Add Project.’ Name your project, then click ‘Add.’

.. image:: media2/image111.png

3. Your project appeared on the list. The project structure automatically opens at once.

.. image:: media2/image32.png
	:scale: 60% 

4. You are in the Project Structure section. Click ‘Add scene’. Name your new scene and select a scene template for it. 

.. image:: media2/image2.png
	:scale: 50% 

5. Now that the scene is added, you can place objects in it. It can be done within or without VR.

Editing without VR (Desktop editor)
===================================

Use your mouse and keyboard to edit VR scenes. The Desktop Editor feature allows editing without VR, namely place objects on the scene and set their positions. To use this feature, click “Edit on desktop.”

.. image:: media2/image11.png

.. image:: media2/image24.png

Capabilities
-------------

- place objects on the scene
- move objects, set precise coordinates (choose the exact location with the help of coordinate axes)
- rotate objects along the chosen axis, set precise parameters
- scale objects along the chosen axis, set precise parameters
- search objects within the list
- delete objects
- Shortcuts
   - Grab: Q
   - Place: W
   - Rotate: E
   - Scale: R 
   - Focus on the object: F
   - Clone objects: Ctrl+C, Ctrl+V 
- Orthographic projection of the scene available
- Go to VR with one button

Controls
--------

**Toolbar**

.. image:: media2/image16.png

1. save
2. cancel action
3. repeat action
4. copy
5. paste
6. move camera
7. move object
8. rotate object
9. scale object

**Rotation toggle**

.. image:: media2/image3.png

Local - the object rotation and movement are relative to its own axis. 
World -  the object rotates and moves accordingly to world space orientation.

**Position toggle**

.. image:: media2/image34.png
	:scale: 85%

Pivot - the object rotates and moves around its pivot point selected by the developer. 
Center -  the object rotates and moves around its geometric center

**Perspective projection toggle**

.. image:: media2/image27.png

The toggle switches the perspective projection between standard and orthographic. 

**Switch to other modes**

.. image:: media2/image1.png

1 - go to VR
2 - go to Desktop View mode*

\*\ To leave the Desktop View mode, press Escape. You’ll receive suggestions to switch to VR, return to Desktop Edit mode, or cancel exiting.

.. image:: media2/image26.png
	:scale: 45%

**Library and objects**

Toggle between the object library and the list of spawned objects. You can also search objects here. 
By selecting a spawned object in the list, you also select it on the scene.

.. image:: media2/image44.png

**Object info**

Here you can see the name, ID and type of the selected object, and parameters of its position. Also, here you can delete the selected object.

.. image:: media2/image10.png

Editing in VR
=============

Video Guide
-----------

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/quGeXpkmm4o" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

1. Click “Edit in VR.” You’ll see the boot screen, then the scene will open in a new window.

.. image:: media2/image23.png

2. Open the VR menu and select an object. 

.. image:: media2/image9.png

`How to use VR controllers <https://varwin.readthedocs.io/en/latest/manuals/controllers.html>`__

*See also:* `VR menu functions <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#vr-menu-functions>`__

3. Place the object in the scene:

   a. Carry it to the desired place,
   b. Press the Trigger button on your controller,
   c. The object appears in the scene; it will hang in the air or stick to the surface.
   d. At the same time, this object will stay in your hand. You can spawn a lot of identical objects by pressing the Trigger button required number of times.
   e. Press the Grip button to remove the object from your hand.

.. image:: media2/image29.png
   :scale: 30%

4. Open the VR menu and save changes. The objects spawned in VR appear in the Varwin RMS interface immediately after you save changes in VR.

.. image:: media2/image28.png
	:scale: 55%  
	
5. Go back to the Project Structure tab. You’ll see the list of spawned objects appeared in the scene. The objects can be renamed.

.. image:: media2/image7.png
   :scale: 60%

VR menu functions
==================

VR main menu functions:

- Save - save changes,
- Undo - cancel an action,
- Redo - repeat an action,
- Filter - open tag list (learn more: `Tags <https://varwin.readthedocs.io/en/latest/platform/rms.html#tags>`__),
- Mode - switch mode (Edit/Preview).

.. image:: media2/image21.png

Composing algorithms
=====================

1. Click ‘Open the visual logic editor.’

.. image:: media2/image5.png

2. Here you can work with the project script.

3. Create a script and click ‘Apply.’ The changes will apply in VR. You can also view the script as code; to do that, click ‘Open code editor.’

.. image:: media2/image14.png
	:scale: 75%

`How to work with Blockly. <https://varwin.readthedocs.io/en/latest/manuals/blockly.html>`__

Preview mode
=============

You can preview each scene of your future project both in VR and on desktop.

Preview in VR
--------------

You can preview your project in VR.

.. image:: media2/image30.png

Preview without VR (Desktop Player)
-----------------------------------

The Desktop Player feature allows previewing both separate scenes and the whole project without VR equipment.

.. image:: media2/image15.png

**Controls:**

- W, A, S, D buttons - movement
- Q - teleportation
- Ctrl - squatting
- Shift - running
- Space - jumping
- LMB - Use (Trigger button analog),
- RMB - Grab (Grip button analog),
- MMB + mouse movement - turning the object
- Left Alt +mouse movement  - moving the object.

App launch configurations
==========================

An app launch configuration is a sequence of the scenes in the project. 

A default configuration is created automatically when the user adds the first scene to the project. Other custom configurations can be added. 

.. image:: media2/image33.png
	:scale: 70%

If there are several scenes in the project, the user can select a start scene.

.. image:: media2/image12.png

Loading scene
--------------

In the App launch configurations section, you can select a loading scene. The users will see it at the moments of transition from one scene to another in the VR. 

To select a loading scene,

- Open your project. Select an app launch configuration. Click Edit,
- Click Loading scene. Select one from the dropdown list,
- Click Apply.

.. image:: media2/image45.png

Selecting a loading scene is optional. If you choose not to do it, the default Varwin scene will be displayed as the loading scene. 

You can create a custom loading scene. It can display information about the loading process, or other data. To learn how to do it, `follow this link. <https://varwin.readthedocs.io/en/latest/sdk/creating-scene-templates.html#creating-a-loading-scene>`__ 

View mode
==========

When the logic is created, you can view your future project. 
In the View mode, you will see the whole project, while the Preview mode refers to separate scenes. 

The View mode can be switched on if the project has at least one `configuration. <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#app-launch-configurations>`__
You can view your project either in VR or on the desktop.

.. image:: media2/image47.png

Building apps out of projects
=============================

When the project is completed, an app for the end user can be built out of it. The apps are exported as .exe files. They can be launched on any PC. 
Varwin platform installation is not needed to launch an exported app. 
Export of the ready apps is not available for Starter Edition.

.. image:: media2/image6.png
