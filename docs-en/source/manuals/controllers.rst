====================
Using VR Controllers
====================

Equipment
=========

Varwin platform can be managed with the following controllers:

- **HTC Vive**

.. image:: media/image2.png
	:scale: 80 %

- **Oculus**

.. image:: media/image3.png

- **Windows Mixed Reality (WMR)** *(any manufacturer)*

.. image:: media/image11.png
	:scale: 50 %

HTC Vive controllers
====================

Grab object
-----------
- Press Grip
- To carry the grabbed object:

 + Preview, View mode: Hold down Grip button
 + Edit mode: No need to hold down Grip button
 
- To put the object down:

 - Preview, View mode: Release Grip button
 - Edit mode: Press Trigger button

.. image:: media/image5.png
	:scale: 45 %  

Action
------
- Press Trigger

.. image:: media/image4.png
	:scale: 45 %  

Teleportation
--------------
- Press the **central area** of trackpad. 
- The pointer will appear. While holding down the trackpad, point the ray to the area where you need to teleport to. If the ray is colored green, teleportation is possible.
- For teleportation, release the trackpad.

.. image:: media/image7.png
	:scale: 45%  

Turn
----
- Press the left side of the trackpad for an incremental turn to the left, right side - for an incremental turn to the right. 

.. image:: media/image6.png
	:scale: 45% 

Menu
----
- Menu button on the left controller:

 - Edit mode: press to display menu, press again to exit menu.
 - Preview mode: displays button for switching to Edit mode.

.. image:: media/image9.png
	:scale: 45% 

Also
---- 
- System button displays Vive system menu (unconnected to Varwin platform)
- If pressed by mistake: press again to get back.

.. image:: media/image8.png
	:scale: 45% 

Oculus controllers
====================

Grab object
-----------
- Press Grip
- To carry the grabbed object:

 - Preview, View mode: Hold down Grip button
 - Edit mode: No need to hold down Grip button
 
- To put the object down:

 - Preview, View mode: Release Grip button
 - Edit mode: Press Trigger button

.. image:: media/image13.png
	:scale: 50% 

Action
-------
- Press Trigger

.. image:: media/image10.png
	:scale: 50% 

Teleportation
--------------
- Press Thumbstick. 
- The pointer will appear. While holding down the thumbstick, point the ray to the area where you need to teleport to. If the ray is colored green, teleportation is possible.
- For teleportation, release the thumbstick.

.. image:: media/image12.png
	:scale: 55% 

Turn
----
- Press thumbstick leftward for an incremental turn to the left, rightward - for an incremental turn to the right. 

.. image:: media/image12.png
	:scale: 55% 

Menu
----
- Y button (left controller) displays the menu.

.. image:: media/image14.png
	:scale: 55% 

WMR controllers
====================

Grab object
-----------
- Press Grab

 - To carry the grabbed object:
 
  - Preview, View mode: Hold down Grab button
  - Edit mode: No need to hold down Grab button
  
 - To put the object down:
 
  - Preview, View mode: Release Grab button
  - Edit mode: Press Trigger button

.. image:: media/image16.png
	:scale: 50% 

Action
-------
- Press Trigger

.. image:: media/image17.png

Teleportation
-------------
- Press the central area of the touchpad. 
- The pointer will appear. While holding down the touchpad, point the ray to the area where you need to teleport to. If the ray is colored green, teleportation is possible.
- For teleportation, release the touchpad.

.. image:: media/image18.png
	:scale: 50% 

Turn
----
- Press the left side of the touchpad for an incremental turn to the left, right side - for an incremental turn to the right. 

.. image:: media/image19.png
	:scale: 50% 

Menu
----

- Menu button on the **left** controller:

 - Edit mode: press to display menu, press again to exit menu, 
 - Preview mode: displays button for switching to Edit mode.
 
.. image:: media/image20.png
	:scale: 50% 

Also
----
- System button displays the Windows system menu (unconnected to Varwin platform)
- If pressed by mistake: press again to get back.

.. image:: media/image1.png
	:scale: 50% 

Further reading
================
`About the VIVE controllers (vive.com) <https://www.vive.com/us/support/vive/category_howto/about-the-controllers.html>`_  

`Controllers in Windows Mixed Reality (support.microsoft.com) <https://support.microsoft.com/en-us/help/4040517/windows-10-controllers-windows-mixed-reality>`_ 

