==================================
Welcome to Varwin's Documentation!
==================================

Varwin is a virtual reality platform that streamlines content development in VR
Perfect for VR studios, enterprise teams and content developers.


.. toctree::
   :caption: Introduction to the Varwin Platform
   :maxdepth: 1
    
   Varwin Platform: General Information <platform/general>
   Equipment and System Requirements <platform/equipment>
   Varwin RMS App <platform/rms>
   Roles and Functions of Varwin Users <platform/roles>
   FAQ: Possible Technical Issues <faq/faq> 
   
.. toctree::
   :caption: Working with the Varwin Platform
   :maxdepth: 1
   
   Using VR Controllers <manuals/controllers>
   Installing Varwin <manuals/install>
   Creating VR Projects <manuals/creating-vr-projects>
   Working with Blockly <manuals/blockly>
   
.. toctree::
   :caption: Working with Unity and the Varwin SDK
   :maxdepth: 1
   
   Varwin SDK: Overview <sdk/sdk-overview> 
   Installing the Varwin SDK <sdk/sdk-install>
   Creating Objects for Varwin in Unity <sdk/creating-objects>
   Creating Scene Templates for Varwin in Unity <sdk/creating-scene-templates>
   Uploading Content from Unity to Varwin Library <sdk/uploading-to-library>
   Creating Libraries with Shared Code <sdk/creating-libraries>
   Objects, Scene Templates Versioning <sdk/versioning>
   Working with Attributes <sdk/attributes>
   Public Interfaces <sdk/public-interfaces>
   
  
   
   

   