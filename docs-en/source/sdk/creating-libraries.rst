===================================
Creating libraries with shared code
===================================

In order to use the same classes for different objects, it’s necessary to create a library to store these classes in. 

1. Create a directory which will contain the classes of the library.

.. image:: images4/image4.png

2. Create an .asmdef file describing the library’s dll. Name it. Add link to the core library, VarwinCore, to Assembly Definition References section. Click Apply to save changes.

.. image:: images4/image6.png
   :scale: 80%
   
.. image:: images4/image5.png
   :scale: 75%

3. Create or add to the directory all the required classes. They have to be stored in the same namespace with a unique name representing the library.

.. image:: images4/image1.png

.. code:: c#

     using Varwin;
     using UnityEngine;
     using Varwin.Public;
     
     namespace Switch.v1;
     {
        [RequireComponent(typeof(Rigidbody))]
        [RequireComponent(typeof(BoxCollider))]
        [RequireComponent(typeof(Animator))]
        
        public class SwitchBehaviour : MonoBehaviour, IUseStartAware
        {
	   public bool ChangeStateOnAnimation = false;
	   private Animator _animator;
	   private bool _state = false; 

4. Go to the .asmdef file of the object which needs to be linked with the created library. Add the link to the library to the Assembly Definition References section. Click Apply to save changes.

.. image:: images4/image2.png
   :scale: 80%

5. Now all the classes from the library can be used for the object linked with it. There can be any number of such objects, and all of them can use the shared code from the library. 

.. image:: images4/image3.png
   :scale: 85%
