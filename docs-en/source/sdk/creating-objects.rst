====================================
Creating Objects for Varwin in Unity
====================================

Video Guides
============

Here you will find the video guides showing how to create objects for the Varwin RMS platform.

**Creating a Button**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/-pbF88u06pI" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

**Creating a Light Bulb**
	
.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/pTngHii9jqY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>	

|

**Creating a Screen**

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/YtiJ_O792dU" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>	

|

Creating objects 
================

1. Create or import a game object and save it as a prefab.
2. Select the prefab in its folder.
3. Open VARWIN SDK -> Create -> Object

.. image:: images1/image8.png

4. Select an object, name it. Add tags for fast search. Fill in the info about the object’s author. 
5. Click Create. You’ll be asked to wait several moments.

.. image:: images1/image1.png

6. Your object has been created. It consists of:
   a. object prefab,
   b. .asmdef file (for object code compilation), 
   c. object class stored in a unique namespace. Should the need arise to add more classes, they will have to be stored in the same directory and the same namespace.

.. image:: images1/image9.png

7. SimpleButton class has been created for the object. It inherits  VarwinObject. This is the main object class, which connects the object with the platform.

.. code:: csharp

    using UnityEngine;
    using Varwin.Public;
    namespace Varwin.Types.Cube_68bc9ae493d843aab141939fcd529513
    {
        [Locale(SystemLanguage.English,"Cube")]
        [Locale(SystemLanguage.Russian,"Cube")]
        public class Cube : VarwinObject
        {
            void Start()
            {
            }

            void Update()
            {
            }
        }
    }
   
8. Everything is ready for writing object code.

Example
-------

The object has a method “ChangeState” in its main class. The method changes bool variable IsPressed.

.. code:: c#

    public void ChangeState()
           {
               isPressed = !isPressed;
               _animator.SetBool("Pressed", isPressed);


To connect this method with the action of pulling the trigger on a controller (“Use”): select method “ChangeState” in Interactable Object Behavior.

.. image:: images1/image3.png

You will have to create a function for Blockly, write an attribute for it (in this case, Checker) and name it.

.. code:: c#

    [Checker("buttonState")]
    [Locale(SystemLanguage.English, "button pressed")]
    public bool ButtonPressedChecker()
    {
       return isPressed;
    }

Should this object have several methods, Blockly will group them within one block within this name. 

- E.g., if we create another state of the button, “is released”,

.. code:: c#

     [Checker("pressed")]
        [Locale(SystemLanguage.English, "is released")]
        public bool IsReleased()
        {
            return !IsPressed;
        }

- Blockly will group these methods this way:

.. image:: images1/image14.png

Everything is ready for object building. Click Build.

.. image:: images1/image15.png

When the object is ready, the folder containing it will open. Now you can upload the object into the Varwin library.

.. image:: images1/image5.png

The object appears in the library.

.. image:: images1/image11.png

Now you can place the object into a scene in VR. Click Save.
Now the object appears in the scene. Open Blockly. You’ll see logic blocks that have been created for the object.
 
.. image:: images1/image16.png

.. image:: images1/image2.png
   :scale: 60%

Working with attributes
------------------------
Ways of working with attributes are described here: `Working with attributes <https://varwin.readthedocs.io/en/latest/sdk/attributes.html>`__

Objects, scene templates versioning
-----------------------------------
Create new versions of existing objects  and scene templates.

`Objects, scene templates versioning: instruction <https://varwin.readthedocs.io/en/latest/sdk/versioning.html>`__







  