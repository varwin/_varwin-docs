============================================
Creating Scene Templates for Varwin in Unity
============================================

Video Guide
===========

This video guide shows how to create a scene template for the Varwin RMS platform.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/o1Gtf1KcnG4" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

|

Creating scene templates
========================

1. Select an asset from Asset Store. Download and import it. 

.. image:: images2/image111.PNG

2. Open the test scene that comes with the asset.

.. image:: images2/image8.png

You can create a scene template for Varwin from virtually any Unity scene with game objects.
Restrictions: 

- The scene can’t have any scripts that don’t belong to Varwin SDK,
- There can be no cameras on the scene.

Don’t forget about light baking.

.. image:: images2/image1.png

3. Mark the colliders to which a player will be able to teleport with TeleportArea tag. 

.. image:: images2/image2.png

4. Now you can build the scene template. Open Varwin SDK - Create - Scene Template.

.. image:: images2/image3.png
	:scale: 80%

5. A window will open where you can name the scene template and give it a description. Fill in the information about the author of the scene template. You’ll also see a preview that will be used as an icon for your scene template. 

.. image:: images2/image9.png

At this stage, World Descriptor that contains 2 objects are automatically added:

- Preview camera - the only camera that can be placed at the scene. You can put it anywhere to get a nice preview.

.. image:: images2/image5.png
	:scale: 55%

- Spawn point - point where the player will appear when the scene template loads. The direction of the blue arrow shows the direction of the player’s eyes at the moment of spawn. 

.. image:: images2/image6.png

6. When everything is ready, click Build and wait for the scene template file to be built.
7. Upload the file into the Varwin library. 

.. image:: images2/image7.png

8. Now you can create a project `(manual: Creating VR Projects) <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html>`__ and use your new scene template in it. 
9. Create new versions of existing objects/scene templates. `Objects, scene templates versioning: instruction <https://varwin.readthedocs.io/en/latest/sdk/versioning.html>`__

Creating a loading scene
========================

You can create a custom `loading scene. <https://varwin.readthedocs.io/en/latest/manuals/creating-vr-projects.html#loading-scene>`__ The users will see it at the moments of transition from one scene to another in the VR. 

The loading scene is created in the same way as any scene template.

You can make the loading scene to display information about the loading process, or other data. To do it, apply script :code:`LoaderFeedBackText.cs` to any Unity object with **Text** or **TextMeshPro** component.