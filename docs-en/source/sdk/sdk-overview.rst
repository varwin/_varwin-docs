==========
Varwin SDK 
==========

What this is
============

SDK (software development kit) is a set of development tools that allows creating apps for certain program suite. 
Varwin SDK is a set of tools for creating content (objects, scene templates) for Varwin in the Unity development platform. In order to work with Varwin SDK, `Unity <https://unity3d.com/get-unity/download/archive?_ga=2.19733694.707766669.1553265932-456492208.1549287338>`__ ver. |unity_version| has to be installed on your PC.

Installation
=============
Varwin SDK comes as a Unity archive file (VarwinSDK.unitypackage). The `link <http://varw.in/getSDK>`__ for downloading the file is attached to the letter you received while `installing the Varwin RMS. <https://varwin.readthedocs.io/en/latest/manuals/install.html#varwin-platform-installation>`_ 

`How to install the Varwin SDK <file:///C:/Git/varwin-docs-en/docs/build/html/sdk/sdk-install.html>`_

Creating objects
================

Create objects for the Varwin platform on Unity. Any prefab can be used as a base for creating an object. 

.. image:: images6/image9.png
	:scale: 70%
	
.. image:: images6/image7.png
	:scale: 75%

`How to create Varwin objects in Unity <https://varwin.readthedocs.io/en/latest/sdk/creating-objects.html>`_

Object file
-----------

.. image:: images6/image6.png

Created objects are packed as files with name suffix .vwo. These files can be uploaded to Varwin.

`How to upload content from Unity to Varwin library <https://varwin.readthedocs.io/en/latest/sdk/uploading-to-library.html>`_

The file contains:

.. image:: images6/image10.png

- bundle - complete model of a certain game object (not including code).
- bundle.manifest - metadata for the bundle.
- bundle.json - technical file; contains a description of the program code and remarks on bundle file contents. 
- bundle.png - object icon. It’s displayed in Blockly and in the spawn menu.
- install.json - file for Blockly. It is used for automatic creation of blocks for Blockly (learn more: Working with Blockly - Blocks for objects).
- .dll files - executable fragments of the object.

Creating scene templates
========================
Create scene templates for the Varwin platform on Unity. 

.. image:: images6/image5.png
	:scale: 70%
	
.. image:: images6/image8.png
	:scale: 70%

Scene template file
-------------------
.. image:: images6/image3.png

Created scene templates are packed as files with name suffix .vwst. These files can be uploaded to Varwin.

Importing models
================

Import models FBX, obj, blend and glTF through Unity into Varwin, one by one or in bulk.
 
.. image:: images6/image2.png
.. image:: images6/image4.png

Specify the object’s name, weight, dimensions, whether it can be moved or used, whether it has physical properties.

Objects, scene templates versioning
===================================

Create new versions of existing objects/scene templates.

`How to create versions of objects, scene templates <https://varwin.readthedocs.io/en/latest/sdk/versioning.html>`_

.. image:: images6/image1.png
	:scale: 70%

Bulk build of objects
=====================

Perform bulk build of all the objects you’ve created for Varwin.

.. image:: images6/image11.png






