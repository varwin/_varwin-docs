=======================
Working with Attributes
=======================

Wrapper class
------------- 

Wrapper class is necessary for transferring VarwinObject to Blockly. In order to obtain an object of this type for a specific VarwinObject use GetWrapper() method.

**Examples**

.. code:: c#

    [Action("Set varwin object")]
    [Locale(SystemLanguage.English,"set varwin object")]
        public void SetSpellbookObject(WrappervObj)
        {
            Spellbook = vObj.GetGameObject().GetComponent<SpellBook>();
        }

.. code:: c#

    [Setter("textSetter")]
    [Getter("textGetter")]
	[Locale(SystemLanguage.English,"text")]
	public Wrapper vObject
	{
        get { return currentVarwinObject.Wrapper();}
	    set { }
    }
   
Logic
-----

Logic attribute: **checker**. Checks true or false.

**Example**

.. code:: c#

    [Checker("buttonState")]
    [Locale(SystemLanguage.English, "button pressed")]
    public bool ButtonPressedChecker()
    {
        return isPressed;
	}
   
Actions
-------
Action with objects: **action** attribute. It can receive or return the following types:

- simple types float, int, bool, string 
- objects of the Wrapper type
- all the above in bulk

**Example**

.. code:: c#

    [Action("setColor")]
           [Values("color")]
           [Locale(SystemLanguage.English,"set color")]
           public void SetColor(string newColor)
           {

               switch (newColor)
               {
                   case "white":
                       _currentColor = Color.white;
   
                       break;
                   case "blue":
                       _currentColor = Color.blue;
   
                       break;
                   case "red":
                       _currentColor = Color.red;
   
                       break;
               }
	}
	
Events
------
The following types can be transferred for the **event** attribute:

- simple types: float, int, bool, string 
- objects of the Wrapper type
- all the above in bulk

**Examples**

.. code:: c#

    [Event("buttonEvent")]
    [Locale(SystemLanguage.English,"button pressed")]
    public event ButtonEventHandler ButtonPressed; 

.. code:: c#

   [Event("buttonEvent")]
   [Locale(SystemLanguage.English,"button unpressed")]
   public event ButtonEventHandler ButtonReleased;
   
Variables
---------
**Variables** can belong to the following types: 

- simple types float, int, bool, string 
- objects of the Wrapper type
- all the above in bulk

get - receive a state
set - appoint a state

**Example**

.. code:: c#

   [Setter("textSetter")]
   [Getter("textGetter")]
   [Locale(SystemLanguage.English,"text")]
   public string DisplayText
       {
           get
           {
           return _text.text;
           }
           set
           {
           _text.text = value;
           }
       }
  
Creating drop-down lists
------------------------
Creating values for the list:

.. code:: c#

    [Value("color")]
    [Locale(SystemLanguage.English,"blue")]
    public string blue;
        
    [Value("color")]
    [Locale(SystemLanguage.English,"red")]
    public string red;
 
 
Using the list:

.. code:: c#

     [Action("setColor")]
     [Values("color")]
     [Locale(SystemLanguage.English,"set color")]
     public void SetColor(string newColor)
     {
         switch (newColor)
         {
                 break;
             case "blue":
                 _currentColor = Color.blue;

                 break;
             case "red":
                 _currentColor = Color.red;

                 break;
         }
 
 









  
  
  
  
  
  
  
















































