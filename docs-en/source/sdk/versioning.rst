===================================
Objects, Scene Templates Versioning
===================================

Before you create the first version of a new object/scene template, you can’t create another version of it.
When you’ve built a new object/scene template, an option of creating another version appears.

.. image:: images5/image1.png
	:scale: 70%

*Object versioning*     

.. image:: images5/image6.png
	:scale: 70%

*Scene template versioning*

When you click Create New Version button, you will be asked to confirm. Then the system will ask if you’d like to build the new version immediately.

.. image:: images5/image7.png

If you don’t confirm immediate building of the new version, the button will become inactive and stay that way until the version is built. To return to building the version, click Build.

.. image:: images5/image4.png
	:scale: 70%