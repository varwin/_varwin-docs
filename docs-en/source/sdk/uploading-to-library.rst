==============================================
Uploading content from Unity to Varwin library 
==============================================
 
1. Open the object in Unity. Click Build.

.. image:: images3/image2.png

2. When the object is ready, the folder containing it will open. Open the Varwin RMS app and drag-and-drop the object into the library.

.. image:: images3/image5.png

3. The object appeared in the library.

.. image:: images3/image3.png

You can perform bulk upload.

.. image:: images3/image1.png

You can simultaneously upload different types of files: objects (.vwo files,) scene templates (.vwst,) metadata (.vwm). You can also upload zip archives with files.

.. image:: images3/image4.png