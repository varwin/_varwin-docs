=========================
Installing the Varwin SDK
=========================

Varwin SDK comes as a Unity archive file (VarwinSDK.unitypackage). Please download it.

`Download Varwin SDK  <http://varw.in/getSDK>`__

.. hint:: You can also find this link in the letter you received while `installing the Varwin RMS <https://varwin.readthedocs.io/en/latest/manuals/install.html#varwin-platform-installation>`__, and in the footer of `Varwin official website <https://varwin.com/>`__.

In order to install SDK:

1. In Unity |unity_version| open Assets - Import Package - Custom Package, and select the archive with SDK that you’ve downloaded.

.. image:: images/image2.png

2. You’ll see a window with a list of elements for installation. Click Import.

.. image:: images/image1.png

3. Then you’ll see a window with the recommended project settings. Click Accept All.

.. image:: images/image4.png

4. When the installation is complete, the Varwin SDK section will appear in the Unity menu.

.. image:: images/image3.png









