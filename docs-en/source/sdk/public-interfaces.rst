=================
Public Interfaces
=================

Method for pressing trigger (Use)
---------------------------------

.. image:: images7/image5.png
   :scale: 65 % 

.. code:: c#

    public interface IUseStartAware
    {
       //Controller interacting with object
       //possible to receive link to gameObject of the controller
       void OnUseStart(UsingContext context);
    }
	
Method for releasing trigger
-----------------------------

.. code:: c#
 
    public interface IUseEndAware
    {
       void OnUseEnd();
    }
	
Method for pressing Grip button (Start of dragging)
---------------------------------------------------

.. image:: images7/image2.png
   :scale: 65 %	

By calling this interface, we switch on dragging of an object.

.. code:: c#

    public interface IGrabStartAware
    {
       //Controller interacting with object
       //possible to receive link to gameObject of the controller
       void OnGrabStart(GrabingContext context);
    }
	
Method for releasing Grip button (End of dragging)
--------------------------------------------------
By calling this interface, we switch off dragging of an object. 

.. code:: c#

    public interface IGrabEndAware
    {
       void OnGrabEnd();
    }
	
Object in hand position
-----------------------

.. image:: images7/image3.png

Specify coordinates of the point where the controller will adhere to object when the object is grabbed.

.. code:: c#

   public interface IGrabPointAware
   {
       //left controller
       Transform GetLeftGrabPoint();
       //right controller
       Transform GetRightGrabPoint();
   }

Method for controller entering object (Touching object with controller)
-----------------------------------------------------------------------

.. image:: images7/image1.png
   :scale: 60 %	

.. code:: c#

    public interface ITouchStartAware
    {
       void OnTouchStart();
    }
	
Method for controller exiting object (End of touch)
---------------------------------------------------

.. code:: c#

    public interface ITouchEndAware
    {
       void OnTouchEnd();
    }
	
Method for clicking on object with pointer
------------------------------------------
.. image:: images7/image4.png
   :scale: 60 %	

.. code:: c#

    public interface IPointerClickAware
    {
       void OnPointerClick();
    }

Method for controller pointer entering object area
---------------------------------------------------
.. code:: c#

    public interface IPointerInAware
    {
       void OnPointerIn();
    }
	
Method for controller pointer exiting object area
-------------------------------------------------
.. code:: c#

    public interface IPointerOutAware
    {
      void OnPointerOut();
    }
	
Subscription for mode switch (Edit, Preview, View)
--------------------------------------------------
.. code:: c#

    public interface ISwitchModeSubscriber
    {
       //mode before - mode after
       void OnSwitchMode(GameMode newMode, GameMode oldMode);
    }



	
	































