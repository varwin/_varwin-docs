==============================
FAQ: Possible technical issues
==============================

Installation and launch
=======================

Can’t install Varwin 
--------------------

1. Check if your PC complies with the `system requirements. <https://varwin.readthedocs.io/en/0.5.2/platform/equipment.html#pc-system-requirements>`__ 
2. Contact Varwin technical support at `support@varwin.com <support@varwin.com>`__. Describe your issue (we recommend to attach a screenshot of the error.) 

Can’t launch Varwin
-------------------

- A yellow dot on the Varwin icon means the program is launching. This may take several minutes.

.. image:: media/image3.png

- A red dot on the icon means an error has occurred during the launch.

.. image:: media/image2.png
 
In this case, try the following (hereinafter, try each successive action if the preceding one hasn’t helped):
 
 1. Open Varwin menu by right- or left-clicking on Varwin icon. Restart Varwin 
 
 .. image:: media/image4.png
 
 
 2. Restart Windows
 
 3. Make sure you’ve allowed access to Varwin services in Windows Firewall\*\
 
\*\ At the first launch, you will get several notifications from Windows Firewall. Select “Allow access” (by doing this, you allow access to certain Varwin services, without which the platform won’t run.

 4. Open Varwin menu, select Show Log

 .. image:: media/image1.png
 
 5. Contact Varwin technical support at `support@varwin.com <support@varwin.com>`__ and send them the log (internet connection required)

Varwin RMS interface is not displayed / is displayed incorrectly 
----------------------------------------------------------------

1. Contact Varwin technical support at `support@varwin.com <support@varwin.com>`__. Describe your issue (we recommend to attach a screenshot of the error.) 

Issues with files
=================

Can’t import a file into the library
------------------------------------

If the file has been archived: make sure the archive was unpacked before the import. 

VR client launch
================

Can’t launch VR client
----------------------

1. Check if your VR equipment and your PC comply with the `system requirements. <https://varwin.readthedocs.io/en/0.5.2/platform/equipment.html#pc-system-requirements>`__
2. Reinstall Varwin platform

VR client is running, but headset displays black screen
-------------------------------------------------------

1. *For Windows Mixed Reality equipment:* check your equipment manual 
2. *For HTC Vive equipment:* check if your base stations are on and you are inside their coverage area
3. Restart Steam VR
4. Restart your PC
5. Check if other VR apps work well with your equipment. 

 a. If yes: contact Varwin technical support at `support@varwin.com <support@varwin.com>`__.
 b. If no: contact customer support of the manufacturer of your equipment.

VR client is running, but nothing happens
-----------------------------------------

Choosing an integrated graphics card in the computer system can cause this error (see screenshot.) It will be seen in the VR. Varwin platform will run the VR client, but it won’t operate.

.. image:: media/image8.png

In this case, switch to discrete graphics:

1. Right-click on Windows desktop, select the NVIDIA control panel from the menu.
2. Select “Manage 3D settings.”
3. Select “Global Settings, then in the section “Preferred graphics processor” select “High-performance processor”.

.. image:: media/image7.png

Other VR client issues
======================

Controllers are not displayed 
-----------------------------

1. Make sure the controllers are charged and switched on. 
2. Restart VR client.
3. Restart Steam VR.

Controllers are displayed in an incorrect place
-----------------------------------------------

1. Move the controllers and headset a little.
2. Restart VR client.
3. Restart Steam VR.
4. Restart your PC
5. Recalibrate your headset (check your equipment manual)

Incorrect floor or horizon level 
--------------------------------

1. Move the controllers and headset a little.
2. Restart VR client.
3. Restart Steam VR.
4. *For Windows Mixed Reality (floor):*

a. In VR, enter Mixed Reality Portal
 
- For that, press the Windows button on your controller, after that click the ‘Home’ button in VR. 
      
.. attention:: Varwin RMS app will be closed.
 
b. Enter ‘All Apps’
c. Run ‘Room Adjustment’ or ‘Floor Adjustment’ (the precise name depends on your Windows version)
d. Rectify floor level, following the instruction that will appear. 

.. image:: media/image6.png

5. For Windows Mixed Reality (horizon): 

 a. Out of VR, open Mixed Reality Portal (it usually runs along with your Varwin VR client, in a separate window)
 b. Click ‘Menu’ button (three lines in the upper left part of the screen)
 c. Select ‘Run Setup’ or ‘Set up Room Boundary’ (the precise name depends on your Windows version)
 d. Rectify your headset settings, following the instruction that will appear. 

.. image:: media/image5.png

6. Recalibrate your headset (check your equipment manual)
7. Restart your PC

VR client crashes (no error notification)
-----------------------------------------

1. Make sure SteamVR / Oculus Rift / Windows Mixed Reality is running
2. Contact Varwin technical support at `support@varwin.com <support@varwin.com>`__. Please attach your logs (‘Show Log’ in Varwin menu)

.. image:: media/image1.png

VR client displays error
========================

Compilation error / Logic execution failure / Logic initialization failure
--------------------------------------------------------------------------

Check your Blockly scenario. Please note: blocks with issues will be illuminated.

Can't read command line arguments / Server disconnected / Can't save / Server is unavailable
--------------------------------------------------------------------------------------------

1. Restart Varwin.
2. Reinstall Varwin.
3. Contact Varwin technical support at `support@varwin.com <support@varwin.com>`__

Can't load object
-----------------

Contact the creator of the object. 

Can't load scene template / Player spawn point not found
--------------------------------------------------------

Contact the creator of the scene template.

World configuration error / Unknown error
-----------------------------------------

Contact Varwin technical support at `support@varwin.com <support@varwin.com>`__. Please attach your logs (‘Show Log’ in Varwin menu)

.. image:: media/image1.png

.. toctree::
